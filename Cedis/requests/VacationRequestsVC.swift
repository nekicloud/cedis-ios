//
//  VacationRequestsVC.swift
//  Cedis
//
//  Created by Administrator on 16/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import ExpandableLabel

var vacationList = [Vacation]()

class VacationRequestsVC:BaseViewController,  UITableViewDelegate, UITableViewDataSource, ExpandableLabelDelegate {

    var states : Array<Bool>!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        vacationTable.delegate = self
        vacationTable.dataSource = self
        
        vacationTable.tableFooterView = UIView()
        vacationTable.separatorColor = UIColor.lightGray
        vacationTable.separatorStyle = .singleLine
        
        self.tabBarController?.tabBar.isHidden = true
        
        vacationTable.rowHeight = UITableViewAutomaticDimension
        vacationTable.estimatedRowHeight = 74
        
        states = [Bool](repeating: true, count: vacationList.count)

    }
    @IBOutlet weak var vacationTable: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        if (CheckConnection.isInternetAvailable() == true){
            getRequests()
        }
        else {
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
            
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func getRequests(){
        
        vacationList = []
        
        let url : String = "http://cedisklik.me/ws/_getVacationRequest.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSArray? {
                        
                        var id: Int?
                        var dateFrom: String?
                        var dateTo: String?
                        var typeName: String?
                        var comment: String?
                        var statusId: Int?
                        var statusName: String?
                        var requestDate: String?
                        
                        var object: NSDictionary?
                        
                        for objSchedule in arraySchedule {
                            
                            object = objSchedule as? NSDictionary
                            
                            id = object!["id"]! as? Int
                            dateFrom = object!["dateFrom"]! as? String
                            dateTo = object!["dateTo"]! as? String
                            typeName = object!["typeName"]! as? String
                            comment = object!["comment"]! as? String
                            statusId = object!["statusId"]! as? Int
                            statusName = object!["statusName"]! as? String
                            requestDate = object!["requestDate"]! as? String
                           
                            vacationList.append(Vacation(id: id!, dateFrom: dateFrom!, dateTo: dateTo!, typeName: typeName!, comment: comment!, statusId: statusId!, statusName: statusName!, requestDate: requestDate!))
                            
                        }
                    }
                    self.states = [Bool](repeating: true, count: vacationList.count)
                    self.vacationTable.reloadData()
                }
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return vacationList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath) as! RequestCell
        
        let dueDate = vacationList[indexPath.section].requestDate
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dueDate2 = dateFormatter2.date(from: dueDate)
        
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: dueDate2!)
        let closingDate = "\(String(format: "%02d", components.day!)).\(String(format: "%02d", components.month!)).\(String(format: "%02d", components.year!))"
        
        c.requestNumber.text = "ZAHTJEV BROJ: \(vacationList[indexPath.section].id)"
        c.requestNumber.font = UIFont(name:"Montserrat-Regular", size: 16.0)
        c.requestDate.text = closingDate
        c.requestPeriod.text = "\(vacationList[indexPath.section].typeName), od: \(vacationList[indexPath.section].dateFrom) do \(vacationList[indexPath.section].dateTo)"
        
        
        c.requestComment.delegate = self
        c.requestComment.ellipsis = NSAttributedString(string: "...")
        
        let strokeTextAttributes: [NSAttributedStringKey: Any] = [
            .foregroundColor : Utils().hexStringToUIColor(hex: Constants.Colors.red),
            .font : UIFont(name:"Montserrat-Regular", size: 18.0)
        ]
        
        c.requestComment.collapsedAttributedLink = NSAttributedString(string: "Prikaži više", attributes: strokeTextAttributes)
        c.requestComment.setLessLinkWith(lessLink: "Prikaži manje", attributes: strokeTextAttributes as [NSAttributedStringKey : AnyObject], position: .right)
        
        c.requestComment.numberOfLines = 7
        c.requestComment.shouldCollapse = true
        c.requestComment.text = vacationList[indexPath.section].comment

        if (vacationList[indexPath.section].statusId == 1) {
            c.requestStatus.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.green);
            c.requestStatus.text = "Odobren"
        } else if (vacationList[indexPath.section].statusId == 2){
            c.requestStatus.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.grey);
            c.requestStatus.text = "Na čekanju"
        } else if (vacationList[indexPath.section].statusId == 3) {
            c.requestStatus.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.red);
            c.requestStatus.text = "Odbijen"
        }
        
        c.requestStatus.layer.masksToBounds = true
        c.requestStatus.layer.cornerRadius = 8
        
        return c
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(section == vacationList.count - 1) {
            return 0.0
        }
        else {
            return 10.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.divider)
        return headerView
    }
    
    //
    // MARK: ExpandableLabel Delegate
    //
    
    func willExpandLabel(_ label: ExpandableLabel) {
        vacationTable.beginUpdates()
    }
    
    func didExpandLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: vacationTable)
        if let indexPath = vacationTable.indexPathForRow(at: point) as IndexPath? {
            states[indexPath.section] = false
            DispatchQueue.main.async { [weak self] in
                self?.vacationTable.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        vacationTable.endUpdates()
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
        vacationTable.beginUpdates()
    }
    
    func didCollapseLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: vacationTable)
        if let indexPath = vacationTable.indexPathForRow(at: point) as IndexPath? {
            states[indexPath.section] = true
            DispatchQueue.main.async { [weak self] in
                self?.vacationTable.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        vacationTable.endUpdates()
    }
    
    

}
