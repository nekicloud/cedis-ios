//
//  RequestCell.swift
//  Cedis
//
//  Created by Administrator on 16/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import ExpandableLabel

class RequestCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        // Initialization code
    }

    @IBOutlet weak var requestNumber: UILabel!
    @IBOutlet weak var requestDate: UILabel!
    @IBOutlet weak var requestPeriod: UILabel!
    @IBOutlet weak var requestStatus: UILabel!
    @IBOutlet weak var requestComment: ExpandableLabel!
    
}
