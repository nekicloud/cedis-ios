//
//  RequestForVacationVC.swift
//  Cedis
//
//  Created by Administrator on 16/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import DropDown
import WWCalendarTimeSelector
import Alamofire

var typesList = [VacationTypes]()


class RequestForVacationVC:BaseViewController, UITextViewDelegate, WWCalendarTimeSelectorProtocol {
    
    let dropDownTypes = DropDown()
    var selectedLine : Int = 0
    var isClosingDate = Bool()
    var dateFromString: String = ""
    var dateToString: String = ""
    
    @IBOutlet weak var scrollVacation: UIScrollView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.registerForKeyboardNotifications()
        self.tabBarController?.tabBar.isHidden = false

    }
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @objc override func keyboardWasShown(notification: NSNotification){
        
        if let infoKey  = notification.userInfo?[UIKeyboardFrameEndUserInfoKey],
            let rawFrame = (infoKey as AnyObject).cgRectValue {
            let keyboardFrame = view.convert(rawFrame, from: nil)
            scrollVacation.contentInset = UIEdgeInsetsMake(0, 0, keyboardFrame.height / 2 + 70, 0)
            let bottomOffset = CGPoint(x: 0, y: keyboardFrame.height)
            scrollVacation.setContentOffset(bottomOffset, animated: true)
        }
    }
    
    @objc override func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        self.view.endEditing(true)
        scrollVacation.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollVacation.setNeedsLayout()
        
        descriptionTV.delegate = self
        
        descriptionTV.layer.borderWidth = 0.5
        descriptionTV.layer.borderColor = UIColor.lightGray.cgColor
        descriptionTV.layer.cornerRadius = 5.0
        
        sendRequestBtn.layer.cornerRadius = 5.0
        
        getVacationTypes()
        
        setupDateFrom()
        setupDateTo()

        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLayoutSubviews() {
        descriptionTV.layer.borderWidth = 0.8
        descriptionTV.layer.borderColor = UIColor.lightGray.cgColor
        descriptionTV.layer.cornerRadius = 5.0
        
        vacationType.layer.borderWidth = 0.8
    }
    
    @IBOutlet weak var vacationType: UIButton!
    
    @IBOutlet weak var descriptionTV: UITextView!
    @IBOutlet weak var dateFrom: UIButton!
    @IBOutlet weak var dateTo: UIButton!
    
    @IBOutlet weak var sendRequestBtn: UIButton!
    @IBAction func openVacationTypes(_ sender: Any) {
        dropDownTypes.show()
    }
    
    @IBAction func openDateFrom(_ sender: Any) {
        isClosingDate = true
        showDatePicker()
    }
    
    @IBAction func openDateTo(_ sender: Any) {
        isClosingDate = false
        showDatePicker()
    }
    
    /* Updated for Swift 4 */
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    /* Older versions of Swift */
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    func getVacationTypes(){
        
        typesList = []
        
        let url : String = "http://cedisklik.me/ws/_vacationTypes.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSArray? {
                        
                        var id: Int?
                        var name: String?
                        
                        var object: NSDictionary?
                        
                        for objSchedule in arraySchedule {
                            
                            object = objSchedule as? NSDictionary
                            
                            id = object!["id"]! as? Int
                            name = object!["name"]! as? String
                            
                            typesList.append(VacationTypes(id: id!, name: name!))
                        }
                    }
                }
                
            /* podesavanje drop down-a*/
                var typesNames: Array <String> = []
                typesNames.append("Izaberite opciju")
                for typesName in typesList {
                    typesNames.append(typesName.name)
                }
                
                self.dropDownTypes.anchorView = self.vacationType
                self.dropDownTypes.dataSource = typesNames
                
                self.vacationType.setTitle("Izaberite opciju", for: .normal)
                self.vacationType.backgroundColor = .clear
                //self.vacationType.layer.borderWidth = 1
                self.vacationType.layer.borderColor = UIColor.lightGray.cgColor
                self.vacationType.setTitleColor(UIColor.gray, for: [])
                self.vacationType.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
                self.vacationType.layer.cornerRadius = 5.0
                
                let appearance = DropDown.appearance()
                
                appearance.cellHeight = 50
                appearance.backgroundColor = UIColor(white: 1, alpha: 1)
                appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
                appearance.cornerRadius = 10
                appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
                appearance.shadowOpacity = 0.9
                appearance.shadowRadius = 25
                appearance.animationduration = 0.25
                appearance.textColor = .darkGray
                
                self.dropDownTypes.selectionAction = { [unowned self] (index, item) in
                    self.vacationType.setTitle(item, for: .normal)
                    
                    if(index > 0) {
                        self.vacationType.setTitleColor(UIColor.darkGray, for: [])
                    } else {
                        self.vacationType.setTitleColor(UIColor.gray, for: [])
                    }
                    self.selectedLine = index
                }
                
            /* podesavanje drop down-a*/
        }
        
    }
    
    func showDatePicker(){
        let selector = WWCalendarTimeSelector.instantiate()
        selector.delegate = self
        
        selector.optionTopPanelBackgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.red)
        selector.optionSelectorPanelBackgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.red)
        selector.optionButtonFontColorDone = Utils().hexStringToUIColor(hex: Constants.Colors.red)
        selector.optionButtonFontColorCancel = Utils().hexStringToUIColor(hex: Constants.Colors.red)
        selector.optionCalendarBackgroundColorTodayHighlight = Utils().hexStringToUIColor(hex: Constants.Colors.red)
        selector.optionCalendarBackgroundColorPastDatesHighlight = Utils().hexStringToUIColor(hex: Constants.Colors.red)
        selector.optionCalendarBackgroundColorFutureDatesHighlight = Utils().hexStringToUIColor(hex: Constants.Colors.red)
        
        self.tabBarController?.tabBar.isHidden = true
        
        present(selector, animated: true, completion: nil)
    }
    
    fileprivate func setupDateFrom() {
        dateFrom.backgroundColor = .clear
        dateFrom.layer.borderWidth = 0.8
        dateFrom.layer.cornerRadius = 5.0
        dateFrom.layer.borderColor = UIColor.lightGray.cgColor
        dateFrom.setTitleColor(UIColor.gray, for: [])
        dateFrom.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
    }
    
    fileprivate func setupDateTo() {
        dateTo.backgroundColor = .clear
        dateTo.layer.borderWidth = 0.8
        dateTo.layer.cornerRadius = 5.0
        dateTo.setTitleColor(UIColor.gray, for: [])
        dateTo.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        dateTo.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        
        self.tabBarController?.tabBar.isHidden = false
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy." //Your New Date format as per requirement change it own
        
        let newDate = dateFormatter.string(from: date) //pass Date here
        //        let selectedDate = dateFormatter.date(from: newDate)
        
        let searchDateFormatter = DateFormatter()
        searchDateFormatter.dateFormat = "yyyy-MM-dd"
        
        if(isClosingDate) {
            dateFrom.setTitle( newDate, for: .normal)
            dateFrom.setTitleColor(UIColor.darkGray, for: [])
            dateFromString = searchDateFormatter.string(from: date)
            
        } else {
            dateTo.setTitle( newDate, for: .normal)
            dateTo.setTitleColor(UIColor.darkGray, for: [])
            dateToString = searchDateFormatter.string(from: date)
        }
        
        //            let dateFormatterNew = DateFormatter()
        //            dateFormatterNew.dateFormat = "yyyy-MM-dd"
        //            let choosenDate = dateFormatterNew.string(from: date)
        
    }
    
    func WWCalendarTimeSelectorCancel(_ selector: WWCalendarTimeSelector, date: Date) {
        self.tabBarController?.tabBar.isHidden = false
        if(isClosingDate) {
            dateFrom.setTitle( "", for: .normal)
            dateFrom.setTitleColor(UIColor.gray, for: [])
            dateFromString = ""
        } else {
            dateTo.setTitle( "", for: .normal)
            dateTo.setTitleColor(UIColor.gray, for: [])
            dateToString = ""
        }
    }
    
    @IBAction func sendRequest(_ sender: Any) {
        
        let vacationType = selectedLine
        let dateFrom = dateFromString
        let dateTo = dateToString
        let comment = descriptionTV.text
        
        let parameters: Parameters = [
            "vacationType" : vacationType,
            "dateFrom" : dateFrom,
            "dateTo" : dateTo,
            "comment" : comment!
        ]
        
        let url : String = "http://cedisklik.me/ws/_sendVacationRequest.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        var error : String = ""
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    var code : String?
                    code = String(describing: requestData["error_code"]!)
                    
                    if code == "1" {
                        
                        let alert1 = UIAlertController(title: "Obavještenje", message: "\(requestData["errors"]!)", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  { action in
                        }))
                        self.present(alert1, animated: true, completion: nil)
                        
                    }
                    else {
                        
                        var data : String?
                        data = String(describing: requestData["data"]!)
                        self.view.endEditing(true)
                        
                        let alert1 = UIAlertController(title: "Obavještenje", message: "\(data!)", preferredStyle: UIAlertControllerStyle.alert)
                        alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  { action in
                            
                            self.dateFrom.setTitle("", for: .normal)
                            self.dateTo.setTitle("", for: .normal)
                            self.dateFromString = ""
                            self.dateToString = ""
                            self.descriptionTV.text = ""
                            
                        }))
                        self.present(alert1, animated: true, completion: nil)
                    }
                }
                else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert9, animated: true, completion: nil)
                }
                
        }
    }
    
    
}
