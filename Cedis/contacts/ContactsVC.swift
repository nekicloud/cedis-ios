//
//  ContactsVC.swift
//  Cedis
//
//  Created by Milos Lalatovic on 17/07/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire
import CustomLoader

class ContactsVC:BaseViewController,  UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchHolder: UIView!
    
    var sections = [String]()
    var dict = [String:[String]]()
    var contacts = [User]()
    
    var filteredSections = [String]()
    var filteredDict = [String:[String]]()
    var filteredContacts = [User]()
    
    let searchBar = UISearchBar()

    var rowNumber = 0
    var searchContacts : Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = false
//        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchHolder.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.red)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorColor = Utils().hexStringToUIColor(hex: Constants.Colors.background);
        tableView.separatorStyle = .singleLine
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
//        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.barTintColor = Utils().hexStringToUIColor(hex:Constants.Colors.red)
        navigationController?.navigationBar.shadowImage = UIImage()
        title = "Imenik";
        
        

        searchBar.placeholder = "Pretraži"
        searchBar.frame = CGRect(x: 0, y: 0, width: (view.frame.size.width), height: 64)
        searchBar.barStyle = .default
        searchBar.isTranslucent = false
        searchBar.barTintColor = Utils().hexStringToUIColor(hex:Constants.Colors.red)
        searchBar.tintColor = .black
        searchBar.backgroundImage = UIImage()
        searchBar.enablesReturnKeyAutomatically = false
        searchBar.returnKeyType = .done

        searchBar.delegate = self
        searchHolder.addSubview(searchBar)
        
        topLayoutGuide.bottomAnchor.constraint(equalTo: searchBar.topAnchor).isActive = true
        
        self.tableView.sectionIndexColor = Utils().hexStringToUIColor(hex: Constants.Colors.background);
        self.tableView.sectionIndexBackgroundColor = UIColor.clear;
        
        getContacts()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(!searchContacts) {
            return sections.count
        } else {
            return filteredSections.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(!searchContacts) {
            return sections[section]
        } else {
            return filteredSections[section]
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(!searchContacts) {
            let key = sections[section]
            if let values = dict[key] {
                return values.count
            }
        } else {
            let key = filteredSections[section]
            if let values = filteredDict[key] {
                return values.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.black
        header.textLabel?.font = UIFont(name:"Montserrat-Bold", size: 18.0)
        header.backgroundColor = UIColor.black
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        rowNumber = indexPath.row
        for i in 0..<indexPath.section {
            rowNumber += self.tableView.numberOfRows(inSection: i)
        }
        performSegue(withIdentifier: "openProfile", sender: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contact", for: indexPath) as! ContactTVC
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        if(!searchContacts) {
            let key = sections[indexPath.section]
            if let values = dict[key.uppercased()] {
                cell.name.text = Utils().formatName(fullName: values[indexPath.row])
            }
        } else {
            let key = filteredSections[indexPath.section]
            if let values = filteredDict[key.uppercased()] {
                cell.name.text = Utils().formatName(fullName: values[indexPath.row])
            }
        }
        
        return cell
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if(!searchContacts) {
            return sections
        } else {
            return filteredSections
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(!searchContacts) {
            if let newScreen = segue.destination as? ProfileVC {
                newScreen.user = self.contacts[rowNumber]
            }
        } else {
            if let newScreen = segue.destination as? ProfileVC {
                newScreen.user = self.filteredContacts[rowNumber]
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if(searchText.count > 0) {
            searchContacts = true
            
            filteredContacts.removeAll()
            filteredDict.removeAll()
            filteredSections.removeAll()
            
            for contact in self.contacts {
                
                var query = searchText.lowercased()
                query = query.trimmingCharacters(in: .whitespacesAndNewlines)
                
                let queries = query.components(separatedBy: " ")
                
                var counter = 0
                for queryPart in queries {
                    if contact.nameSurname.lowercased().range(of:queryPart) != nil {
                        counter = counter + 1
                    }
                }
                
                if(counter == queries.count) {
                    filteredContacts.append(contact)
                }
                
            }
            sortContacts(users: filteredContacts)
        } else {
            searchContacts = false
            sortContacts(users: contacts)
            
            searchBar.endEditing(true)
        }
    }
    
    func getContacts() {
        
        var error : String = ""
        LoadingView.standardProgressBox.show(inView: self.view)
        
        let url : String = "http://cedisklik.me/ws/_contacts.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, method: .get, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                let responseData = response.result.value as! NSDictionary
                let errorCode : String? = String(describing: responseData["error_code"]!)
                self.view.removeLoadingViews(animated: true)
                
                if errorCode == "1" {
                    
                }
                else if errorCode == "0" {
                    
                    if let array = responseData["data"] as! NSArray? {
                        
                        var object: NSDictionary?
                        for obj in array  {
                            
                            object = obj as? NSDictionary
                            
                            self.contacts.append(User(id: 0,
                                                      nameSurname: (object!["nameSurname"]! as? String)!,
                                                      jmbg: "",
                                                      email: (object!["email"]! as? String)!,
                                                      phone: (object!["phone"]! as? String)!,
                                                      profileImage: (object!["profileImage"]! as? String)!,
                                                      workPlace: (object!["workPlace"]! as? String)!,
                                                      role: 0,
                                                      passwordChanged: 0,                                                      
                                                      sector: (object!["sector"]! as? String)!,
                                                      apiKey: "",
                                                      deviceId: "",
                                                      token: ""))
                            
                        }
                        self.sortContacts(users: self.contacts)
                    }
                }
        }
    }
    
    func sortContacts(users : [User]) {
        
        for contact in users {
            
            let name = contact.nameSurname
            
            let key = "\(name[name.startIndex])"
            let lower = key.uppercased()
            
            if(!searchContacts) {
                if var values = self.dict[lower] {
                    values.append(name)
                    self.dict[lower] = values
                } else {
                    self.dict[lower] = [name]
                }
            } else {
                if var values = self.filteredDict[lower] {
                    values.append(name)
                    self.filteredDict[lower] = values
                } else {
                    self.filteredDict[lower] = [name]
                }
            }
        }
        
        if(!searchContacts) {
            self.sections = [String] (self.dict.keys)
            self.sections = self.sections.sorted()
        } else {
            self.filteredSections = [String] (self.filteredDict.keys)
            self.filteredSections = self.filteredSections.sorted()
        }
        
        self.tableView.reloadData()
    }
    
}


