//
//  ProfileVC.swift
//  Cedis
//
//  Created by Milos Lalatovic on 17/07/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit

class ProfileVC:BaseViewController {
    @IBOutlet weak var separator2: UIView!
    
    @IBOutlet weak var separator1: UIView!
    @IBOutlet weak var emailHolder: UIView!
    @IBOutlet weak var phoneHolder: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var job: UILabel!
    @IBOutlet weak var sector: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var email: UILabel!
    

    @IBAction func call(_ sender: Any) {
        if((phoneNumber.text?.count)! > 0) {
            phoneNumber.text?.makeACall()
        }
    }
    
    @IBAction func sendSMS(_ sender: Any) {
        if((phoneNumber.text?.count)! > 0) {
            let number = "sms:\(phoneNumber.text!)"
            let newString = number.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
            let sms = URL(string: newString)!
            UIApplication.shared.open(sms, options: [:], completionHandler: nil)
        }
    }
    @IBAction func sendEmail(_ sender: Any) {
        if((email.text?.count)! > 0) {
            if let url = URL(string: "mailto:\(email.text!)") {
                UIApplication.shared.open(url)
            }
        }
    }
    
    //stupid init
    var user : User = userList[0]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.shadowImage = UIImage()
        
        name.text = Utils().formatName(fullName: user.nameSurname)
        job.text = user.workPlace
        sector.text = user.sector
        
        if(user.phone.count > 0) {
            phoneNumber.text = user.phone
        } else {
            phoneHolder.isHidden = true
            separator1.isHidden = true
            separator2.isHidden = true
        }
        
        if(user.email.count > 0) {
            email.text = user.email
        } else {
            emailHolder.isHidden = true
            separator1.isHidden = true
            separator2.isHidden = true
        }
        
        
        
        
        profileImage.setShowActivityIndicator(true)
        profileImage.setIndicatorStyle(.gray)
        profileImage.sd_setImage(with: URL(string: "\(user.profileImage)"), placeholderImage: UIImage(named:"user"), options: .refreshCached)
    }
}

extension String {
    
    enum RegularExpressions: String {
        case phone = "^\\s*(?:\\+?(\\d{1,3}))?([-. (]*(\\d{3})[-. )]*)?((\\d{3})[-. ]*(\\d{2,4})(?:[-.x ]*(\\d+))?)\\s*$"
    }
    
    func isValid(regex: RegularExpressions) -> Bool {
        return isValid(regex: regex.rawValue)
    }
    
    func isValid(regex: String) -> Bool {
        let matches = range(of: regex, options: .regularExpression)
        return matches != nil
    }
    
    func onlyDigits() -> String {
        let filtredUnicodeScalars = unicodeScalars.filter{CharacterSet.decimalDigits.contains($0)}
        return String(String.UnicodeScalarView(filtredUnicodeScalars))
    }
    
    func makeACall() {
        if isValid(regex: .phone) {
            if let url = URL(string: "tel://\(self.onlyDigits())"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
}
