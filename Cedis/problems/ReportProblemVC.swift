//
//  ReportProblemVC.swift
//  Cedis
//
//  Created by Administrator on 17/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import CoreLocation
import SystemConfiguration

var problemList = [ProblemTypes]()
var userLongitude: Double = 0.00
var userLatitude: Double = 0.00

class ReportProblemVC:BaseViewController, UITextViewDelegate, CLLocationManagerDelegate {
    
    let dropDownTypes = DropDown()
    var selectedID : Int = 0
    var selectedLine : String = ""
    var locationValue = false
    var locationManager:CLLocationManager!
    
    @IBOutlet weak var problemScroll: UIScrollView!
    
    @IBAction func openProblems(_ sender: Any) {
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        problemDescription.delegate = self
        
        problemDescription.layer.borderWidth = 0.8
        problemDescription.layer.borderColor = UIColor.lightGray.cgColor
        problemDescription.layer.cornerRadius = 5.0
        
        sendProblemBtn.layer.cornerRadius = 5.0
        
        getProblemTypes()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.registerForKeyboardNotifications()
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.tabBarController?.tabBar.isHidden = false
        self.deregisterFromKeyboardNotifications()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func determineMyCurrentLocation() {
        
        let authorizationStatus = CLLocationManager.authorizationStatus()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        if (authorizationStatus == .authorizedWhenInUse) {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
        
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            //locationManager.startUpdatingHeading()
        }
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        // print("user latitude = \(userLocation.coordinate.latitude)")
        //print("user longitude = \(userLocation.coordinate.longitude)")
        
        userLatitude = userLocation.coordinate.latitude
        userLongitude = userLocation.coordinate.longitude
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    /* Older versions of Swift */
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBOutlet weak var sendProblemBtn: UIButton!
    @IBOutlet weak var problemType: UIButton!
    @IBOutlet weak var problemDescription: UITextView!
    @IBOutlet weak var checkLocation: UIButton!
    
    @IBAction func showProblems(_ sender: Any) {
        dropDownTypes.show()
    }
    
    @IBAction func allowLocation(_ sender: Any) {
        
        if(locationValue) {
            locationValue = false
            checkLocation.setImage(UIImage(named: "check.png"), for: .normal)
        } else {
            locationValue = true
            checkLocation.setImage(UIImage(named: "check_box.png"), for: .normal)
            determineMyCurrentLocation()
            
        }
        
    }
    
    @IBAction func sendProblem(_ sender: Any) {
        
        let descriptionString = problemDescription.text
        let latitude = userLatitude
        let longitude = userLongitude
        
        let parameters: Parameters = [
            "problemType": selectedID,
            "problemTitle": selectedLine,
            "latitude": latitude,
            "longitude": longitude,
            "comment": descriptionString!
        ]
        
        let url : String = "http://cedisklik.me/ws/_reportTheProblem.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        var error : String = ""
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    var code : String?
                    code = String(describing: requestData["error_code"]!)
                    
                    if code == "1" {
                        
                        let alert1 = UIAlertController(title: "Obavještenje", message: "\(requestData["errors"]!)", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  { action in
                        }))
                        self.present(alert1, animated: true, completion: nil)
                        
                    }
                    else {
                        
                        var data : String?
                        data = String(describing: requestData["data"]!)
                        
                        let alert1 = UIAlertController(title: "Obavještenje", message: "\(data!)", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  { action in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert1, animated: true, completion: nil)
                    }
                }
                else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert9, animated: true, completion: nil)
                }
                
        }
        
        
    }
    
    @objc override func keyboardWasShown(notification: NSNotification){
        
        if let infoKey  = notification.userInfo?[UIKeyboardFrameEndUserInfoKey],
            let rawFrame = (infoKey as AnyObject).cgRectValue {
            let keyboardFrame = view.convert(rawFrame, from: nil)
            problemScroll.contentInset = UIEdgeInsetsMake(0, 0, keyboardFrame.height / 2 + 110, 0)
            let bottomOffset = CGPoint(x: 0, y: keyboardFrame.height / 2 + 30)
            problemScroll.setContentOffset(bottomOffset, animated: true)
        }
    }
    
    @objc override func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        self.view.endEditing(true)
        problemScroll.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
    }
    
    func getProblemTypes(){
        problemList = []
        
        let url : String = "http://cedisklik.me/ws/_problemTypes.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSArray? {
                        
                        var id: Int?
                        var name: String?
                        
                        var object: NSDictionary?
                        
                        for objSchedule in arraySchedule {
                            
                            object = objSchedule as? NSDictionary
                            
                            id = object!["id"]! as? Int
                            name = object!["name"]! as? String
                            
                            problemList.append(ProblemTypes(id: id!, name: name!))
                        }
                    }
                }
                
                /* podesavanje drop down-a*/
                var typesNames: Array <String> = []
                typesNames.append("Izaberite opciju")
                for typesName in problemList {
                    typesNames.append(typesName.name)
                }
                
                self.dropDownTypes.anchorView = self.problemType
                self.dropDownTypes.dataSource = typesNames
                
                self.problemType.setTitle("Izaberite opciju", for: .normal)
                self.problemType.backgroundColor = .clear
                self.problemType.layer.borderWidth = 0.8
                self.problemType.layer.borderColor = UIColor.lightGray.cgColor
                self.problemType.setTitleColor(UIColor.gray, for: [])
                self.problemType.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
                self.problemType.layer.cornerRadius = 5.0
                
                let appearance = DropDown.appearance()
                
                appearance.cellHeight = 50
                appearance.backgroundColor = UIColor(white: 1, alpha: 1)
                appearance.selectionBackgroundColor = UIColor(red: 0.6494, green: 0.8155, blue: 1.0, alpha: 0.2)
                appearance.cornerRadius = 10
                appearance.shadowColor = UIColor(white: 0.6, alpha: 1)
                appearance.shadowOpacity = 0.9
                appearance.shadowRadius = 25
                appearance.animationduration = 0.25
                appearance.textColor = .darkGray
                
                self.dropDownTypes.selectionAction = { [unowned self] (index, item) in
                    self.problemType.setTitle(item, for: .normal)
                    
                    if(index > 0) {
                        self.problemType.setTitleColor(UIColor.darkGray, for: [])
                        self.selectedID = problemList[index - 1].id
                        self.selectedLine = problemList[index - 1].name
                    } else {
                        self.problemType.setTitleColor(UIColor.gray, for: [])
                    }
                }
                
                /* podesavanje drop down-a*/
        }
    }
    
}
