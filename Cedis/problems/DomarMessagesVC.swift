//
//  ConversationMessagesVC.swift
//  Cedis
//
//  Created by Administrator on 18/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire

class DomarMessagesVC: BaseViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    var reloadSelectedID: Int?
    var refreshControl = UIRefreshControl()
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        
        navTitle.title = problemConversationList[problemConversationSelected!].title
        self.registerForKeyboardNotifications()
        self.tableViewScrollToBottom(animated: true)
        seen()
        
        if(problemConversationList[problemConversationSelected!].archived > 0) {
            newMessage.isEnabled = false
        }
        
        messagesTable.rowHeight = UITableViewAutomaticDimension
        messagesTable.estimatedRowHeight = 44
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        messagesTable.delegate = self
        messagesTable.dataSource = self
        newMessage.delegate = self
        
        messagesTable.tableFooterView = UIView()
        messagesTable.separatorColor = UIColor.clear
        
        newMessageView.layer.borderWidth = 0.8
        newMessageView.layer.borderColor = UIColor.lightGray.cgColor
        newMessageView.layer.cornerRadius = 5.0
        
        //        refreshControl.attributedTitle = NSAttributedString(string: "")
        //        refreshControl.addTarget(self, action: "refresh", for: UIControlEvents.valueChanged)
        //        messagesTable.addSubview(refreshControl)
    }
    
    func refresh(sender:AnyObject) {
        getMessages()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        self.deregisterFromKeyboardNotifications()
    }
    
    @IBOutlet weak var messagesTable: UITableView!
    @IBOutlet weak var navTitle: UINavigationItem!
    @IBOutlet weak var newMessage: UITextField!
    @IBOutlet weak var newMessageView: UIView!
    @IBOutlet weak var commentViewConst: NSLayoutConstraint!
    @IBOutlet weak var tableViewConst: NSLayoutConstraint!
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true);
        return false;
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (problemConversationList.count == 0){
            return 0
        }else{
            return problemConversationList[problemConversationSelected!].messages.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "ConversationMessagesCell", for: indexPath) as! HRMessageTVC
        
        if (problemConversationList.count != 0){
            
            c.timestamp.text = formatDate(date: problemConversationList[problemConversationSelected!].messages[indexPath.row].timestamp)
            
            if (problemConversationList[problemConversationSelected!].messages[indexPath.row].messageType == 1 ){
                
                if((indexPath.row > 1) && (problemConversationList[problemConversationSelected!].messages[indexPath.row - 1].messageType == 1)) {
                    c.stack.isHidden = true
                    c.messageTop.constant = 10.0
                    c.stack.frame = CGRect(x: c.stack.frame.origin.x, y: c.stack.frame.origin.y, width: 0.0, height: 0.0)
                } else {
                    c.stack.isHidden = false
                    c.messageTop.constant = 45.0
                }
                
                c.senderName.text = Utils().formatName(fullName: userList[0].nameSurname)
                c.senderName.textColor = UIColor.black
                c.messageText.backgroundColor = UIColor(displayP3Red: 246/255, green: 248/255, blue: 248/255, alpha: 1)
            }
            else if (problemConversationList[problemConversationSelected!].messages[indexPath.row].messageType == 2){
                
                if((indexPath.row > 1) && (problemConversationList[problemConversationSelected!].messages[indexPath.row - 1].messageType == 2)) {
                    c.stack.isHidden = true
                    c.messageTop.constant = 10.0
                    c.stack.frame = CGRect(x: c.stack.frame.origin.x, y: c.stack.frame.origin.y, width: 0.0, height: 0.0)
                } else {
                    c.stack.isHidden = false
                    c.messageTop.constant = 45.0
                }
                
                c.senderName.text = "Odgovor domara"
                c.senderName.textColor = UIColor(displayP3Red: 212/255, green: 53/255, blue: 53/255, alpha: 1)
                c.messageText.backgroundColor = UIColor(displayP3Red: 226/255, green: 228/255, blue: 228/255, alpha: 1)
            }
            c.messageText.text = problemConversationList[problemConversationSelected!].messages[indexPath.row].message
            c.messageText.layer.cornerRadius = 4
            c.messageText.layer.masksToBounds = true
            
            //            let fixedWidth = c.messageText.frame.size.width
            //            let newSize = c.messageText.sizeThatFits(CGSize(width: fixedWidth, height: c.messageText.contentSize.height))
            //            c.messageText.frame.size = CGSize(width: fixedWidth, height: newSize.height)
            
        }
        
        return c
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 5.0
        }
        else {
            return 0.0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc override func keyboardWasShown(notification: NSNotification){
        
        if let infoKey  = notification.userInfo?[UIKeyboardFrameEndUserInfoKey],
            let rawFrame = (infoKey as AnyObject).cgRectValue {
            let keyboardFrame = view.convert(rawFrame, from: nil)
            commentViewConst.constant = keyboardFrame.size.height + 5.0
            //            tableViewConst.constant = -58.0 - keyboardFrame.size.height
            UIView.animate(withDuration: 0.5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc override func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        self.view.endEditing(true)
        
        commentViewConst.constant = 30.0
        //        tableViewConst.constant = -58.0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        
        
        if(problemConversationList[problemConversationSelected!].archived == 0) {
            
            let messageString = self.newMessage.text
            let conversationId = problemConversationList[problemConversationSelected!].conversationId
            
            let parameters: Parameters = [
                "conversationId": conversationId,
                "message": messageString!
            ]
            
            let url : String = "http://cedisklik.me/ws/_problemConversationResponse.php"
            
            let headers = [
                "Authorization": "ApiKey \(userList[0].apiKey)",
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            
            var error : String = ""
            
            Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers)
                .validate()
                .responseJSON { (response) -> Void in
                    
                    if let requestData = response.result.value as! NSDictionary? {
                        
                        var code : String?
                        code = String(describing: requestData["error_code"]!)
                        
                        if code == "1" {
                            let alert1 = UIAlertController(title: "Obavještenje", message: "\(requestData["errors"]!)", preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  { action in
                                self.navigationController?.popViewController(animated: true)
                            }))
                            self.present(alert1, animated: true, completion: nil)
                        }  else {
                            
                            //                        var data : String?
                            //                        data = String(describing: requestData["data"]!)
                            //
                            //                        let alert1 = UIAlertController(title: "Obavještenje", message: "\(data!)", preferredStyle: UIAlertControllerStyle.alert)
                            //
                            //                        alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  { action in
                            
                            self.view.endEditing(true)
                            self.newMessage.text = ""
                            self.getMessages()
                            self.tableViewScrollToBottom(animated: true)
                            //self.navigationController?.popViewController(animated: true)
                            //                        }))
                            //                        self.present(alert1, animated: true, completion: nil)
                        }
                    }
                    else {
                        let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                        alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                        self.present(alert9, animated: true, completion: nil)
                    }
                    
            }
        } else {
            let alert1 = UIAlertController(title: "Obavještenje", message: "Ovaj problem je arhiviran", preferredStyle: UIAlertControllerStyle.alert)
            alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
            self.present(alert1, animated: true, completion: nil)
        }
        
    }
    
    func tableViewScrollToBottom(animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            let numberOfSections = 1
            
            if numberOfSections > 0 {
                let numberOfRows = self.messagesTable.numberOfRows(inSection: numberOfSections-1)
                
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                    self.messagesTable.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                }
            }
        }
    }
    
    func getMessages(){
        problemConversationList = []
        problemMessagesList = []
        
        let url : String = "http://cedisklik.me/ws/_getProblemMessages.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSArray? {
                        
                        var conversationId: Int?
                        var title: String?
                        var timestamp: String?
                        var status: Int?
                        var archived: Int?
                        var objectMessages: NSDictionary?
                        
                        var object: NSDictionary?
                        
                        for objSchedule in arraySchedule {
                            
                            object = objSchedule as? NSDictionary
                            
                            conversationId = object!["conversationId"]! as? Int
                            title = object!["title"]! as? String
                            timestamp = object!["timestamp"]! as? String
                            status = object!["status"]! as? Int
                            archived = object!["archived"]! as? Int
                            
                            if let messagesObj = object!["messages"]! as? NSArray{
                                
                                var id: Int?
                                var message: String?
                                var messageType: Int?
                                var timestamp: String?
                                
                                for objM in messagesObj {
                                    
                                    objectMessages = objM as? NSDictionary
                                    
                                    id = objectMessages!["id"]! as? Int
                                    message = objectMessages!["message"]! as? String
                                    messageType = objectMessages!["messageType"]! as? Int
                                    timestamp = objectMessages!["timestamp"]! as? String
                                    
                                    problemMessagesList.append(Messages(id: id!, message: message!, messageType: messageType!, timestamp: timestamp!))
                                    
                                }
                                
                            }
                            
                            problemConversationList.append(Conversations(conversationId: conversationId!, title: title!, timestamp: timestamp!, status: status!, archived: archived!, messages: problemMessagesList))
                            problemMessagesList = []
                            
                        }
                    }
                    self.messagesTable.reloadData()
                }
        }
    }
    
    func seen() {
        let url : String = "http://cedisklik.me/ws/_problemSeen.php?conversationId=\(problemConversationList[problemConversationSelected!].conversationId)"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    var code : String?
                    code = String(describing: requestData["error_code"]!)
                    
                    if code == "1" {
                        let alert1 = UIAlertController(title: "Obavještenje", message: "\(requestData["errors"]!)", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  { action in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert1, animated: true, completion: nil)
                    }
                }
                else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert9, animated: true, completion: nil)
                }
        }
    }
    
    
}
