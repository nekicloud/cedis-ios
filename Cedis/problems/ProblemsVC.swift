//
//  ConversationsVC.swift
//  Cedis
//
//  Created by Administrator on 18/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire

var problemConversationList = [Conversations]()
var problemMessagesList = [Messages]()
var problemConversationSelected: Int?

class ProblemsVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)

        if (CheckConnection.isInternetAvailable() == true){
            getConversations()
        }
        else {
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        conversationsTable.delegate = self
        conversationsTable.dataSource = self
        
        conversationsTable.tableFooterView = UIView()
        conversationsTable.separatorColor = UIColor.lightGray

    }

    @IBOutlet weak var conversationsTable: UITableView!
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return problemConversationList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "ContactCell", for: indexPath) as! ContactCell
        
        c.contactTitle.text = problemConversationList[indexPath.section].title
        c.contactMessage.text = problemConversationList[indexPath.section].messages[problemConversationList[indexPath.section].messages.count - 1].message
        
        if (problemConversationList[indexPath.section].status == 0){
            c.readIcon.image = UIImage(named: "unread")
        }
        else {
            c.readIcon.image = UIImage(named: "read")
        }
        
        return c
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(section == problemConversationList.count - 1){
            return 0
        }
        else {
           return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor(displayP3Red: 212/255, green: 53/255, blue: 53/255, alpha: 1)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        problemConversationSelected = indexPath.section
        performSegue(withIdentifier: "showMessages", sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func getConversations() {
        problemConversationList = []
        problemMessagesList = []
        
        let url : String = "http://cedisklik.me/ws/_getProblemMessages.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSArray? {
                        
                        var conversationId: Int?
                        var title: String?
                        var timestamp: String?
                        var status: Int?
                        var archived: Int?
                        var objectMessages: NSDictionary?
                        
                        var object: NSDictionary?
                        
                        for objSchedule in arraySchedule {
                            
                            object = objSchedule as? NSDictionary
                            
                            conversationId = object!["conversationId"]! as? Int
                            title = object!["title"]! as? String
                            timestamp = object!["timestamp"]! as? String
                            status = object!["status"]! as? Int
                            archived = object!["archived"]! as? Int
                            
                            if let messagesObj = object!["messages"]! as? NSArray{
                                
                                var id: Int?
                                var message: String?
                                var messageType: Int?
                                var timestamp: String?
                                
                                for objM in messagesObj {
                                    
                                    objectMessages = objM as? NSDictionary
                                    
                                    id = objectMessages!["id"]! as? Int
                                    message = objectMessages!["message"]! as? String
                                    messageType = objectMessages!["messageType"]! as? Int
                                    timestamp = objectMessages!["timestamp"]! as? String
                                    
                                    problemMessagesList.append(Messages(id: id!, message: message!, messageType: messageType!, timestamp: timestamp!))
                                    
                                }
                                
                            }
                            
                            problemConversationList.append(Conversations(conversationId: conversationId!, title: title!, timestamp: timestamp!, status: status!, archived: archived!, messages: problemMessagesList))
                            problemMessagesList = []
                            
                        }
                    }
                        self.conversationsTable.reloadData()
                
                }
        }
    }

}
