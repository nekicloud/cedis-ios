import UIKit




class YearPayrollsVC:BaseViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tableView: UITableView!
    var payrollForThisYear = [Payroll]()
    
    var row : Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .clear
        
        tableView.delegate = self
        tableView.dataSource = self

        tableView.separatorColor = .clear
        tableView.separatorStyle = .none
        tableView.reloadData()
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return payrollForThisYear.count
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        row = indexPath.row
        
        performSegue(withIdentifier: "showPayroll", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let newScreen = segue.destination as? ShowDocumentVC {
            newScreen.documentTitle = "\(payrollForThisYear[row].month), \(payrollForThisYear[0].year)"
            newScreen.documentUrl = payrollForThisYear[row].link as NSString
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "payroll", for: indexPath) as! PayrollTVC
    
//        cell.holder.dropShadow(color: .black, opacity: 0.5, offSet: CGSize(width: -1, height: 1), radius: 5, scale: true)

        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.month.text = payrollForThisYear[indexPath.row].month
        cell.month.textColor = Utils().hexStringToUIColor(hex: Constants.Colors.red)
        
        return cell
    }
}
