//
//  PayrollViewController.swift
//  Cedis
//
//  Created by Milos Lalatovic on 19/07/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import SwipeMenuViewController

class PayrollViewController: SwipeMenuViewController {
    
    @IBOutlet weak var tabsDivider: UIView!
    var payrolls = [Payroll]()
    var uniqueBasedOnYear = [Payroll]()
    
    var options = SwipeMenuViewOptions()
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Platna lista"
        
        payrolls.append(Payroll(link: "https://dev.amplitudo.me/cedis/documents/TEST_CEDIS.docx", year: 2018, month: "Januar"));
        payrolls.append(Payroll(link: "https://dev.amplitudo.me/cedis/documents/TEST_CEDIS.docx", year: 2018, month: "Februar"));
        payrolls.append(Payroll(link: "https://dev.amplitudo.me/cedis/documents/TEST_CEDIS.docx", year: 2018, month: "Mart"));
        payrolls.append(Payroll(link: "https://dev.amplitudo.me/cedis/documents/TEST_CEDIS.docx", year: 2018, month: "April"));
        payrolls.append(Payroll(link: "https://dev.amplitudo.me/cedis/documents/TEST_CEDIS.docx", year: 2018, month: "Maj"));
        payrolls.append(Payroll(link: "https://dev.amplitudo.me/cedis/documents/TEST_CEDIS.docx", year: 2018, month: "Jun"));
        payrolls.append(Payroll(link: "https://dev.amplitudo.me/cedis/documents/TEST_CEDIS.docx", year: 2017, month: "Januar"));
        payrolls.append(Payroll(link: "https://dev.amplitudo.me/cedis/documents/TEST_CEDIS.docx", year: 2016, month: "Januar"));
        payrolls.append(Payroll(link: "https://dev.amplitudo.me/cedis/documents/TEST_CEDIS.docx", year: 2015, month: "Januar"));
        payrolls.append(Payroll(link: "https://dev.amplitudo.me/cedis/documents/TEST_CEDIS.docx", year: 2017, month: "Februar"));

        payrolls = payrolls.sorted(by: { $0.year > $1.year })
        
        uniqueBasedOnYear = payrolls.unique{$0.year}
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)

        uniqueBasedOnYear.forEach { data in
            
            let vc = storyboard.instantiateViewController(withIdentifier: "yearPayrolls") as! YearPayrollsVC
            
            vc.title = "\(data.year)"
            
            var payrollsPerYear = [Payroll]()
            
            payrolls.forEach { payroll in
                if(data.year == payroll.year) {
                    payrollsPerYear.append(payroll)
                }
            }
            
            vc.payrollForThisYear = payrollsPerYear
            self.addChildViewController(vc)
        }

        //        options.tabView.needsAdjustWidth = true
        options.tabView.height = 60
        options.tabView.style = .segmented
        options.tabView.itemView.selectedTextColor = Utils().hexStringToUIColor(hex: Constants.Colors.dark)
        options.tabView.itemView.textColor = Utils().hexStringToUIColor(hex: Constants.Colors.grey)
//        options.tabView.itemView.  = Utils().hexStringToUIColor(hex: Constants.Colors.gray)
        options.tabView.addition = .none
        
        reload()
        
        view.bringSubview(toFront: tabsDivider)
        tabsDivider.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.grey)

        
    }
    
    
    
    private func reload() {
        swipeMenuView.reloadData(options: options)
    }
    
    // MARK: - SwipeMenuViewDelegate
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewWillSetupAt currentIndex: Int) {
        super.swipeMenuView(swipeMenuView, viewWillSetupAt: currentIndex)
        print("will setup SwipeMenuView")
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewDidSetupAt currentIndex: Int) {
        super.swipeMenuView(swipeMenuView, viewDidSetupAt: currentIndex)
        print("did setup SwipeMenuView")
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, willChangeIndexFrom fromIndex: Int, to toIndex: Int) {
        super.swipeMenuView(swipeMenuView, willChangeIndexFrom: fromIndex, to: toIndex)
        print("will change from section\(fromIndex + 1)  to section\(toIndex + 1)")
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, didChangeIndexFrom fromIndex: Int, to toIndex: Int) {
        super.swipeMenuView(swipeMenuView, didChangeIndexFrom: fromIndex, to: toIndex)
        print("did change from section\(fromIndex + 1)  to section\(toIndex + 1)")
    }
    
    
    // MARK - SwipeMenuViewDataSource
    
    override func numberOfPages(in swipeMenuView: SwipeMenuView) -> Int {
        return uniqueBasedOnYear.count
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, titleForPageAt index: Int) -> String {
        return childViewControllers[index].title ?? ""
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewControllerForPageAt index: Int) -> UIViewController {
        
        let vc = childViewControllers[index]
        vc.didMove(toParentViewController: self)
        return vc
    }
}

extension Array {
    func unique<T:Hashable>(by: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(by(value)) {
                set.insert(by(value))
                arrayOrdered.append(value)
            }
        }
        
        return arrayOrdered
    }
}
