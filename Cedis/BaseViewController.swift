//
//  BaseViewController.swift
//  Cedis
//
//  Created by Milos Lalatovic on 06/09/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.barTintColor = Utils().hexStringToUIColor(hex: Constants.Colors.red)
        
        let attributes = [NSAttributedStringKey.font:
            UIFont(name:"Montserrat-Bold", size: 18.0),
                          NSAttributedStringKey.foregroundColor: UIColor.white] as! [NSAttributedStringKey: Any]
        navigationController?.navigationBar.titleTextAttributes = attributes
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.white], for: .selected)
        //        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = .white
    }
    
    
    func registerForKeyboardNotifications(){
        //Adding notifies on keyboard appearing
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func deregisterFromKeyboardNotifications(){
        //Removing notifies on keyboard appearing
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        
    }
    
    
    func formatDate(date: String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dueDate2 = dateFormatter.date(from: date)
        
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day], from: dueDate2!)
        let sendDate = "\(String(format: "%02d", components.day!)).\(String(format: "%02d", components.month!)).\(String(format: "%02d", components.year!))"
        
         return sendDate
    }
    

}
