//
//  ConfirmationsVC
//  Cedis
//
//  Created by Administrator on 16/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import ExpandableLabel


class ConfirmationsVC:BaseViewController,  UITableViewDelegate, UITableViewDataSource, ExpandableLabelDelegate {

    var states : Array<Bool>!
    var list = [Confirmation]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        confirmaitionsTable.delegate = self
        confirmaitionsTable.dataSource = self
        
        confirmaitionsTable.tableFooterView = UIView()
        confirmaitionsTable.separatorColor = UIColor.lightGray
        confirmaitionsTable.separatorStyle = .singleLine
        
        self.tabBarController?.tabBar.isHidden = true
        
        confirmaitionsTable.rowHeight = UITableViewAutomaticDimension
        confirmaitionsTable.estimatedRowHeight = 74
        
        states = [Bool](repeating: true, count: list.count)

    }
    @IBOutlet weak var confirmaitionsTable: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        if (CheckConnection.isInternetAvailable() == true){
            getRequests()
        }
        else {
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
            
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    func getRequests(){
        
        list = []
        
        let url : String = "http://cedisklik.me/ws/_getConfirmationRequest.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSArray? {
                        
                        var id: Int
                        var vrsta_potvrde: String
                        var broj_mjeseci: String
                        var namjena_potvrde: String
                        var statusId: Int
                        var vrijeme: String
                        var ime_banke: String
                        var sjediste_banke: String
                        var broj_rata: String
                        var ime_drustva: String
                        var sjediste_drustva: String
                        var ostalo: String
                        
                        var object: NSDictionary?
                        
                        for objSchedule in arraySchedule {
                            
                            object = objSchedule as? NSDictionary
                            
                            id = (object!["id"]! as? Int)!
                            vrsta_potvrde = (object!["vrsta_potvrde"]! as? String)!
                            broj_mjeseci = (object!["broj_mjeseci"]! as? String)!
                            namjena_potvrde = (object!["namjena_potvrde"]! as? String)!
                            statusId = (object!["statusId"]! as? Int)!
                            vrijeme = (object!["vrijeme"]! as? String)!
                            ime_banke = (object!["ime_banke"]! as? String)!
                            sjediste_banke = (object!["sjediste_banke"]! as? String)!
                            broj_rata = (object!["broj_rata"]! as? String)!
                            ime_drustva = (object!["ime_drustva"]! as? String)!
                            sjediste_drustva = (object!["sjediste_drustva"]! as? String)!
                            ostalo = (object!["ostalo"]! as? String)!
                           
                            self.list.append(Confirmation(id: id, vrsta_potvrde: vrsta_potvrde, broj_mjeseci: broj_mjeseci, namjena_potvrde: namjena_potvrde, statusId: statusId, vrijeme: vrijeme, ime_banke: ime_banke, sjediste_banke: sjediste_banke, broj_rata: broj_rata, ime_drustva: ime_drustva, sjediste_drustva: sjediste_drustva, ostalo: ostalo))
                            
                        }
                    }
                    self.states = [Bool](repeating: true, count: self.list.count)
                    self.confirmaitionsTable.reloadData()
                }
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath) as! RequestCell
        
        let dueDate = list[indexPath.section].vrijeme
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dueDate2 = dateFormatter2.date(from: dueDate)
        
        let calender = Calendar.current
        let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: dueDate2!)
        let closingDate = "\(String(format: "%02d", components.day!)).\(String(format: "%02d", components.month!)).\(String(format: "%02d", components.year!))"
        
        c.requestNumber.text = "ZAHTJEV BROJ: \(list[indexPath.section].id)"
        c.requestNumber.font = UIFont(name:"Montserrat-Regular", size: 16.0)
        c.requestDate.text = closingDate
        
        
        
        var title = "";
        title.append(list[indexPath.section].vrsta_potvrde);
        if(!list[indexPath.section].broj_mjeseci.isEmpty) {
            title.append(", za " + list[indexPath.section].broj_mjeseci);
        }
        c.requestPeriod.text = title
        
        
        var content = ""
        if(list[indexPath.section].namjena_potvrde.isEmpty) {
            content.append(list[indexPath.section].ostalo);
        } else {
            content.append(list[indexPath.section].namjena_potvrde);
        }
        
        if(!list[indexPath.section].broj_rata.isEmpty) {
            content.append(", " + list[indexPath.section].broj_rata + " rata");
            content.append("\n" + list[indexPath.section].ime_drustva + ", " + list[indexPath.section].sjediste_drustva);
        } else if (!list[indexPath.section].ime_banke.isEmpty){
            content.append("\n" + list[indexPath.section].ime_banke + ", " + list[indexPath.section].sjediste_banke);
        }
        
        c.requestComment.text = content

        
        
        
        c.requestComment.delegate = self
        c.requestComment.ellipsis = NSAttributedString(string: "...")
        
        let strokeTextAttributes: [NSAttributedStringKey: Any] = [
            .foregroundColor : Utils().hexStringToUIColor(hex: Constants.Colors.red),
            .font : UIFont(name:"Montserrat-Regular", size: 18.0)
        ]
        
        c.requestComment.collapsedAttributedLink = NSAttributedString(string: "Prikaži više", attributes: strokeTextAttributes)
        c.requestComment.setLessLinkWith(lessLink: "Prikaži manje", attributes: strokeTextAttributes as [NSAttributedStringKey : AnyObject], position: .right)
        
        c.requestComment.numberOfLines = 7
        c.requestComment.shouldCollapse = true

        if (list[indexPath.section].statusId == 1) {
            c.requestStatus.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.green);
            c.requestStatus.text = "Odobren"
        } else if (list[indexPath.section].statusId == 2){
            c.requestStatus.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.grey);
            c.requestStatus.text = "Na čekanju"
        } else if (list[indexPath.section].statusId == 3) {
            c.requestStatus.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.red);
            c.requestStatus.text = "Odbijen"
        }
        
        c.requestStatus.layer.masksToBounds = true
        c.requestStatus.layer.cornerRadius = 8
        
        return c
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(section == list.count - 1) {
            return 0.0
        }
        else {
            return 10.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.divider)
        return headerView
    }
    
    //
    // MARK: ExpandableLabel Delegate
    //
    
    func willExpandLabel(_ label: ExpandableLabel) {
        confirmaitionsTable.beginUpdates()
    }
    
    func didExpandLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: confirmaitionsTable)
        if let indexPath = confirmaitionsTable.indexPathForRow(at: point) as IndexPath? {
            states[indexPath.section] = false
            DispatchQueue.main.async { [weak self] in
                self?.confirmaitionsTable.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        confirmaitionsTable.endUpdates()
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
        confirmaitionsTable.beginUpdates()
    }
    
    func didCollapseLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: confirmaitionsTable)
        if let indexPath = confirmaitionsTable.indexPathForRow(at: point) as IndexPath? {
            states[indexPath.section] = true
            DispatchQueue.main.async { [weak self] in
                self?.confirmaitionsTable.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        confirmaitionsTable.endUpdates()
    }
    
    

}
