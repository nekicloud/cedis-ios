//
//  NewConfirmationVC.swift
//  Cedis
//
//  Created by Milos Lalatovic on 26/12/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import DLRadioButton

class NewConfirmationVC: BaseViewController, UITextFieldDelegate {
    
    var checks = 0;
    var otherButtons : [DLRadioButton] = [];

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true);
        return false;
    }
    
    let dropDown1 = DropDown()
    let dropDown2 = DropDown()
    let dropDownMonths = DropDown()

    var selectedMonthOption = 0;
    var stringsMonths: Array <String> = []

    var strings2: Array <String> = []
    var selectedPurposeOption = 0;

//    @IBOutlet weak var spinner1: UIButton!
//    @IBAction func openSpinner1(_ sender: Any) {
//        dropDown1.show()
//    }
    
    
    @IBOutlet weak var holder1: UIView!
    @IBOutlet weak var holder2: UIView!
    @IBOutlet weak var holder3: UIView!
    @IBOutlet weak var holder4: UIView!
    @IBOutlet weak var holder5: UIView!
    
    
    @IBOutlet weak var holder4top: NSLayoutConstraint!
    @IBOutlet weak var purpuse: UIView!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var spinner2: UIButton!
    @IBAction func openSpinner2(_ sender: Any) {
        dropDown2.show()
    }
    
    @IBOutlet weak var layout1: UIView!
    @IBOutlet weak var layout2: UIView!
    @IBOutlet weak var layoutOther: UIView!
    
    @IBOutlet weak var bankName: UITextField!
    @IBOutlet weak var bankCity: UITextField!
    @IBOutlet weak var installmentCount: UITextField!
    @IBOutlet weak var companyName: UITextField!
    @IBOutlet weak var companyCity: UITextField!
    @IBOutlet weak var otherForm: UITextField!
    
    @IBOutlet weak var mButtons: UIStackView!
    
    @IBOutlet weak var submit: UIButton!
    @IBAction func sendRequest(_ sender: Any) {
        send();
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCheck(holder: holder1, text: "Radno pravnom statusu", tag: 10)
        setupCheck(holder: holder2, text: "Visini mjesečne zarade", tag: 20)
        setupCheck(holder: holder3, text: "Prosječnoj zaradi za: ", tag: 30)
        setupCheck(holder: holder4, text: "Visini obustava na zaradi", tag: 40)
        setupCheck(holder: holder5, text: "Ostalo", tag: 50)
        
        setupSpinners()
        setupTexts()

    }
    
    func setupTexts() {
        
        self.bankName.delegate = self;
        self.bankCity.delegate = self;
        self.installmentCount.delegate = self;
        self.companyName.delegate = self;
        self.companyCity.delegate = self;
        self.otherForm.delegate = self;
        
        self.bankName.autocapitalizationType = .sentences
        self.bankCity.autocapitalizationType = .sentences
        self.installmentCount.autocapitalizationType = .sentences
        self.companyName.autocapitalizationType = .sentences
        self.companyCity.autocapitalizationType = .sentences
        self.otherForm.autocapitalizationType = .sentences
        
        bankName.addTarget(self, action: #selector(bankNameDidChange(_:)),
                            for: UIControlEvents.editingChanged)
        bankCity.addTarget(self, action: #selector(bankCityDidChange(_:)),
                           for: UIControlEvents.editingChanged)
        installmentCount.addTarget(self, action: #selector(installmentCountDidChange(_:)),
                           for: UIControlEvents.editingChanged)
        companyName.addTarget(self, action: #selector(companyNameDidChange(_:)),
                           for: UIControlEvents.editingChanged)
        companyCity.addTarget(self, action: #selector(companyCityDidChange(_:)),
                           for: UIControlEvents.editingChanged)
//        otherForm.addTarget(self, action: #selector(otherFormDidChange(_:)),
//                           for: UIControlEvents.editingChanged)
        
    }
    
    @objc func bankNameDidChange(_ textField: UITextField) {
        if ((bankCity.text?.count)! > 2) {
            if ((textField.text?.count)! > 2) {
                submit.isHidden = false;
            } else {
                submit.isHidden = true;
            }
        }
    }
    @objc func bankCityDidChange(_ textField: UITextField) {
        if ((bankName.text?.count)! > 2) {
            if ((textField.text?.count)! > 2) {
                submit.isHidden = false;
            } else {
                submit.isHidden = true;
            }
        }
    }
    @objc func installmentCountDidChange(_ textField: UITextField) {
        
        if ((companyCity.text?.count)! > 2 && (companyName.text?.count)! > 2) {
            if ((textField.text?.count)! > 0) {
                submit.isHidden = false;
            } else {
                submit.isHidden = true;
            }
        }
    }
    @objc func companyNameDidChange(_ textField: UITextField) {
        
        if ((installmentCount.text?.count)! > 0 && (companyCity.text?.count)! > 2) {
            if ((textField.text?.count)! > 2) {
                submit.isHidden = false;
            } else {
                submit.isHidden = true;
            }
        }
    }
    @objc func companyCityDidChange(_ textField: UITextField) {
        
        if ((installmentCount.text?.count)! > 0 && (companyName.text?.count)! > 2) {
            if ((textField.text?.count)! > 2) {
                submit.isHidden = false;
            } else {
                submit.isHidden = true;
            }
        }
    }


    func setupCheck(holder: UIView, text: String, tag: Int) {
        
        holder.layer.borderWidth = 1
        holder.layer.borderColor = UIColor.lightGray.cgColor
        holder.layer.cornerRadius = 5.0
        
        var radioButton = DLRadioButton(frame: CGRect(x: 20, y: 0, width: holder.frame.size.width - 40, height: 50))
    
        radioButton.isMultipleSelectionEnabled = true
        radioButton.isIconOnRight = true
        
        radioButton.setTitle(text ,for: .normal)
        
        radioButton.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
        
        radioButton.setTitleColor(Utils().hexStringToUIColor(hex: "c5c4c4"), for: .normal);
        radioButton.setTitleColor(Utils().hexStringToUIColor(hex: "1e303c"), for: .selected);
        radioButton.iconColor = Utils().hexStringToUIColor(hex: "c5c4c4");
        radioButton.icon = UIImage.init(named: "ic_checkbox")!
        radioButton.iconSelected = UIImage.init(named: "ic_checkbox_selected")!
        radioButton.iconSize = 30.0
        
        //            radioButton.indicatorColor = Utils().hexStringToUIColor(hex: "C72028");
        radioButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left;
        //        radioButton.marginWidth = 20.0
                radioButton.tag = tag
                radioButton.addTarget(self, action: #selector(logSelectedButton), for: UIControlEvents.touchUpInside);
        holder.addSubview(radioButton)
    }
    
    
    @objc func logSelectedButton(radioButton : DLRadioButton) {
        
        if(radioButton.tag == 10 || radioButton.tag == 20 || radioButton.tag == 40) {
            
            if(radioButton.isSelected) {
                checks = checks + 1;
                title2.isHidden =  false
                spinner2.isHidden =  false
            } else {
                checks = checks - 1;
                if (checks == 0) {
                    title2.isHidden =  true
                    spinner2.isHidden =  true
                    layout2.isHidden =  true
                    layout1.isHidden =  true
                    submit.isHidden =  true
                    dropDown2.selectRow(at: 0);
                }
            }
            
        } else if (radioButton.tag == 30) {
        
            if(radioButton.isSelected) {
                
                self.mButtons.isHidden = false;
                setupMonths(text: 3);
                setupMonths(text: 6);
                setupMonths(text: 9);
                setupMonths(text: 12);
                
                self.holder4top.constant = 70
                
                checks = checks + 1;
                title2.isHidden =  false
                spinner2.isHidden =  false
            } else {
                
                if let theLabel = self.view.viewWithTag(30) as? UIButton {
                    theLabel.setTitle("Prosječnoj zaradi za: " ,for: .normal)
                }
                        
                self.mButtons.isHidden = true;
                self.mButtons.removeArrangedSubview(self.view.viewWithTag(3)!)
                self.mButtons.removeArrangedSubview(self.view.viewWithTag(6)!)
                self.mButtons.removeArrangedSubview(self.view.viewWithTag(9)!)
                self.mButtons.removeArrangedSubview(self.view.viewWithTag(12)!)
                self.holder4top.constant = 20
                
                checks = checks - 1;
                if (checks == 0) {
                    title2.isHidden =  true
                    spinner2.isHidden =  true
                    layout2.isHidden =  true
                    layout1.isHidden =  true
                    submit.isHidden =  true
                    dropDown2.selectRow(at: 0);
                }
            }
            
            
        } else if (radioButton.tag == 50) {
            
            if(radioButton.isSelected) {
                layoutOther.isHidden =  false
                title2.isHidden =  false
                spinner2.isHidden =  false
                checks = checks + 1;
            } else {
                layoutOther.isHidden =  true
                checks = checks - 1;
                if (checks == 0) {
                    title2.isHidden =  true
                    spinner2.isHidden =  true
                    layout2.isHidden =  true
                    layout1.isHidden =  true
                    submit.isHidden =  true
                    dropDown2.selectRow(at: 0);
                }
            }
        }
    }
    
    func setupMonths(text: Int) {
        
        var radioButton = DLRadioButton(frame: CGRect(x: (text / 3 - 1) * 85 + 15, y: 10, width: 75, height: 40))
        
        radioButton.isMultipleSelectionEnabled = false
        radioButton.backgroundColor = Utils().hexStringToUIColor(hex: "c5c4c4")
        radioButton.layer.cornerRadius = 5.0
        radioButton.clipsToBounds = true

        radioButton.setTitle("\(text)" ,for: .normal)
        radioButton.contentHorizontalAlignment = .center
        
        radioButton.setTitleColor(Utils().hexStringToUIColor(hex: "1e303c"), for: .normal);
        radioButton.setTitleColor(Utils().hexStringToUIColor(hex: "C72028"), for: .selected);
        
        
        radioButton.iconSize = 0.0
        radioButton.tag = text
        radioButton.addTarget(self, action: #selector(logSelectedMonth), for: UIControlEvents.touchUpInside);
        
        radioButton.otherButtons = otherButtons
        otherButtons.append(radioButton)
        
        mButtons.addSubview(radioButton)
    }
    
    @objc func logSelectedMonth(radioButton : DLRadioButton) {
        if(radioButton.isSelected) {
            print(radioButton.tag)
            
            if let theLabel = self.view.viewWithTag(30) as? UIButton {
                if(radioButton.tag == 3) {
                    theLabel.setTitle("Prosječnoj zaradi za: \(radioButton.tag) mjeseca" ,for: .normal)
                } else {
                    theLabel.setTitle("Prosječnoj zaradi za: \(radioButton.tag) mjeseci" ,for: .normal)
                }
            }
            
        }
        
    }
    
    
    func setupSpinners() {
        
        strings2.append("Odaberite namjenu")
        strings2.append("Zaključivanja ugovora o kreditu sa bankom")
        strings2.append("Kupovina na kredit")
        
        self.dropDown2.anchorView = self.spinner2
        self.dropDown2.dataSource = strings2
        
        self.spinner2.setTitle("Odaberite vrstu potvrde", for: .normal)
        self.spinner2.backgroundColor = .white
        self.spinner2.layer.borderWidth = 1
        self.spinner2.layer.borderColor = UIColor.lightGray.cgColor
        self.spinner2.setTitleColor(UIColor.gray, for: [])
        self.spinner2.contentEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        self.spinner2.layer.cornerRadius = 5.0
        
        self.dropDown2.selectionAction = { [unowned self] (index, item) in
            self.spinner2.setTitle(item, for: .normal)
            self.selectedPurposeOption = index
            
            if(index > 0) {
                self.spinner2.setTitleColor(UIColor.darkGray, for: [])
                
                if (index == 1) {
                    
                    self.layout1.isHidden = false;
                    self.layout2.isHidden = true;
                } else if (index == 2) {
                    
                    self.layout2.isHidden = false;
                    self.layout1.isHidden = true;
                }
                
            } else {
                self.spinner2.setTitleColor(UIColor.gray, for: [])
                self.submit.isHidden = true
                
                self.layout2.isHidden = true;
                self.layout1.isHidden = true;
            }
        }
        
        
        
    
    }
    
    func send() {
        
        var month = ""
        if(selectedMonthOption > 0) {
            month = stringsMonths[selectedMonthOption]
        }
        
        var purpose = ""
        if(selectedPurposeOption > 0) {
            purpose = strings2[selectedPurposeOption]
        }
        
        var request = ""
        
        if let checkbox1 = self.view.viewWithTag(10) as? DLRadioButton {
            if(checkbox1.isSelected) {
                request.append((checkbox1.titleLabel?.text)!);
            }
        }
        if let checkbox2 = self.view.viewWithTag(20) as? DLRadioButton {
            if(checkbox2.isSelected) {
                if(request.count != 0) {
                    request.append(", ");
                }
                request.append((checkbox2.titleLabel?.text)!);
            }
        }
        if let checkbox3 = self.view.viewWithTag(30) as? DLRadioButton {
            if(checkbox3.isSelected) {
                if(request.count != 0) {
                    request.append(", ");
                }
                request.append((checkbox3.titleLabel?.text)!);
            }
        }
        if let checkbox4 = self.view.viewWithTag(40) as? DLRadioButton {
            if(checkbox4.isSelected) {
                if(request.count != 0) {
                    request.append(", ");
                }
                request.append((checkbox4.titleLabel?.text)!);
            }
        }
        if let checkbox5 = self.view.viewWithTag(50) as? DLRadioButton {
            if(checkbox5.isSelected) {
                if(request.count != 0) {
                    request.append(", ");
                }
                request.append((checkbox5.titleLabel?.text)! + " " + otherForm.text!);
            }
        }
        
       
        
        
        let parameters: Parameters = [
            "vrsta_potvrde": request,
            "broj_mjeseci": "",
            "namjena_potvrde": purpose,
            "ime_banke": bankName.text!,
            "sjediste_banke": bankCity.text!,
            "broj_rata": installmentCount.text!,
            "ime_drustva": companyName.text!,
            "sjediste_drustva": companyCity.text!,
            "ostalo": ""
        ]
        
        
        let url : String = "http://cedisklik.me/ws/_sendConfirmationRequest.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        var error : String = ""
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    var code : String?
                    code = String(describing: requestData["error_code"]!)
                    
                    if code == "1" {
                        
                        let alert1 = UIAlertController(title: "Obavještenje", message: "\(requestData["errors"]!)", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  { action in
                        }))
                        self.present(alert1, animated: true, completion: nil)
                        
                    }
                    else {
                        
                        var data : String?
                        data = String(describing: requestData["data"]!)
                        self.view.endEditing(true)
                        
                        let alert1 = UIAlertController(title: "Obavještenje", message: "\(data!)", preferredStyle: UIAlertControllerStyle.alert)
                        alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  { action in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert1, animated: true, completion: nil)
                    }
                }
                else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert9, animated: true, completion: nil)
                }
                
        }
    }

}
