//
//  InstagramTVC.swift
//  Cedis
//
//  Created by Milos Lalatovic on 14/08/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import ExpandableLabel

class InstagramTVC: UITableViewCell {

    @IBOutlet weak var post: ExpandableLabel!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var holder: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        holder.layer.cornerRadius = 5
        holder.layer.shadowColor = UIColor.black.cgColor
        holder.layer.shadowOpacity = 0.5
        holder.layer.shadowOffset = CGSize(width: -1, height: 1)
        holder.layer.shadowRadius = 5
        holder.layer.masksToBounds = false
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
