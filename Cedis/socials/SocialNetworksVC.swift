//
//  SocialNetworksVC.swift
//  Cedis
//
//  Created by Milos Lalatovic on 13/08/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import SwipeMenuViewController


class SocialNetworksVC: SwipeMenuViewController {

    @IBOutlet weak var tabsDivider: UIView!
    var options = SwipeMenuViewOptions()
    var pages : Int = 0

    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Društvene mreže"
        setupViews()
        // Do any additional setup after loading the view.
    }

    private func reload() {
        swipeMenuView.reloadData(options: options)
    }
    
    // MARK: - SwipeMenuViewDelegate
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewWillSetupAt currentIndex: Int) {
        super.swipeMenuView(swipeMenuView, viewWillSetupAt: currentIndex)
        print("will setup SwipeMenuView")
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewDidSetupAt currentIndex: Int) {
        super.swipeMenuView(swipeMenuView, viewDidSetupAt: currentIndex)
        print("did setup SwipeMenuView")
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, willChangeIndexFrom fromIndex: Int, to toIndex: Int) {
        super.swipeMenuView(swipeMenuView, willChangeIndexFrom: fromIndex, to: toIndex)
        print("will change from section\(fromIndex + 1)  to section\(toIndex + 1)")
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, didChangeIndexFrom fromIndex: Int, to toIndex: Int) {
        super.swipeMenuView(swipeMenuView, didChangeIndexFrom: fromIndex, to: toIndex)
        print("did change from section\(fromIndex + 1)  to section\(toIndex + 1)")
    }
    
    
    // MARK - SwipeMenuViewDataSource
    
    override func numberOfPages(in swipeMenuView: SwipeMenuView) -> Int {
        return pages
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, titleForPageAt index: Int) -> String {
        return childViewControllers[index].title ?? ""
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewControllerForPageAt index: Int) -> UIViewController {
        
        let vc = childViewControllers[index]
        vc.didMove(toParentViewController: self)
        return vc
    }
    
    func setupViews() {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Socials", bundle: nil)
        
        
        let vcON = storyboard.instantiateViewController(withIdentifier: "facebook") as! FacebookVC
        vcON.title = "Facebook"
        self.addChildViewController(vcON)
        
        let vcOFF = storyboard.instantiateViewController(withIdentifier: "instagram") as! InstagramVC
        vcOFF.title = "Instagram"
        self.addChildViewController(vcOFF)
        
        //        options.tabView.needsAdjustWidth = true
        options.tabView.height = 60
        options.tabView.style = .segmented
        options.tabView.itemView.selectedTextColor = Utils().hexStringToUIColor(hex: Constants.Colors.dark)
        options.tabView.itemView.textColor = Utils().hexStringToUIColor(hex: Constants.Colors.grey)
//                options.tabView.itemView  = Utils().hexStringToUIColor(hex: Constants.Colors.gray)
        options.tabView.addition = .none
        
        pages = 2
        reload()
        
        view.bringSubview(toFront: tabsDivider)
        tabsDivider.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.grey)
    }

}
