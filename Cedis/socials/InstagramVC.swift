//
//  InstagramViewController.swift
//  Cedis
//
//  Created by Milos Lalatovic on 13/08/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire

class InstagramVC:BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    var posts = [InstagramPost]()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        view.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.white)
        
        tableView.separatorColor = .clear
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        
        
        if (CheckConnection.isInternetAvailable()) {
            posts = []
            getPosts()
        } else {
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
        }    }
    
    func getPosts(){
        
        let url : String = "https://api.instagram.com/v1/users/self/media/recent/?access_token=5411913143.2f99088.7e8c5193a8a14c1eb83ccf15630cfffd"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSArray? {
                        
                        var object: NSDictionary?
                        
                        for objSchedule in arraySchedule {
                            object = objSchedule as? NSDictionary
                            
                            var images = object!["images"] as! NSDictionary;
                            var standard_resolution = images["standard_resolution"] as! NSDictionary;
                            
                            var captionValue = "";
                            
                            if let value = (object!["caption"] as? NSDictionary) {
                                captionValue = value["text"] as! String
                            } else {
                                //NULL
                            }
                            self.posts.append(InstagramPost(created_time: (object!["created_time"]! as? String)!,
                                                            caption: captionValue,
                                                            type: (object!["type"]! as? String)!,
                                                            link: (object!["link"]! as? String)!,
                                                            photoUrl: (standard_resolution["url"]! as? String)!))
                        }
                    }
                    self.tableView.reloadData()
                    
                }
                else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { action in
                        //hud1.hide(animated: true, afterDelay: 0)
                    }))
                    self.present(alert9, animated: true, completion: nil)
                }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "instagram", for: indexPath) as! InstagramTVC
        c.selectionStyle = .none

        c.post.text = posts[indexPath.row].caption
        
        c.photo.setShowActivityIndicator(true)
        c.photo.setIndicatorStyle(.gray)
        c.photo.sd_setImage(with: URL(string: "\(posts[indexPath.row].photoUrl)"))
        
        return c
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let url = URL(string: posts[indexPath.row].link)
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    }
}
