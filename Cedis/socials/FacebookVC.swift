//
//  FacebookViewController.swift
//  Cedis
//
//  Created by Milos Lalatovic on 13/08/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire
import ExpandableLabel

class FacebookVC:BaseViewController, UITableViewDelegate, UITableViewDataSource, ExpandableLabelDelegate {
    
    var states : Array<Bool>!
    
    @IBOutlet weak var tableView: UITableView!
    var posts = [FacebookPost]()

    override func viewDidLoad() {
        super.viewDidLoad()

        states = [Bool](repeating: true, count: posts.count)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        view.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.white)
        
        tableView.separatorColor = .clear
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        
        
        if (CheckConnection.isInternetAvailable()) {
            posts = []
            getPosts()
        } else {
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
        }
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    
    func getPosts(){
        
        let url : String = "http://cedisklik.me/ws/_getFacebookPosts.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSArray? {
                        
                        var object: NSDictionary?
                        
                        for objSchedule in arraySchedule {
                            object = objSchedule as? NSDictionary
                            
                            var link = ""
                            if(object!["link"] != nil) {
                                link = (object!["link"]! as? String)!
                            }
                            
                            var full_picture = ""
                            if(object!["full_picture"] != nil) {
                                full_picture = (object!["full_picture"]! as? String)!
                            }
                            
                            var message = ""
                            if(object!["message"] != nil) {
                                message = (object!["message"]! as? String)!
                            }
                            
                            //TODO
                            self.posts.append(FacebookPost(link: link,
                                                      message: message,
                                                      full_picture: full_picture,
                                                      id: (object!["id"]! as? String)!))
                                
                            
                        }
                        
                    }
                    self.states = [Bool](repeating: true, count: self.posts.count)
                    self.tableView.reloadData()
                    
                }
                else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { action in
                        //hud1.hide(animated: true, afterDelay: 0)
                    }))
                    self.present(alert9, animated: true, completion: nil)
                }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "facebook", for: indexPath) as! FacebookTVC
        c.selectionStyle = .none

        
        c.post.delegate = self
        c.post.ellipsis = NSAttributedString(string: "...")
        
        let strokeTextAttributes: [NSAttributedStringKey: Any] = [
            .foregroundColor : Utils().hexStringToUIColor(hex: Constants.Colors.red),
            .font : UIFont(name:"Montserrat-Regular", size: 18.0)
        ]
        
        c.post.collapsedAttributedLink = NSAttributedString(string: "Prikaži više", attributes: strokeTextAttributes)
        c.post.setLessLinkWith(lessLink: "Prikaži manje", attributes: strokeTextAttributes as [NSAttributedStringKey : AnyObject], position: .right)

        c.post.numberOfLines = 7
        c.post.shouldCollapse = true
        c.post.text = posts[indexPath.row].message
        
        c.layoutIfNeeded()
        
        if(posts[indexPath.row].full_picture != "") {
            c.photo.isHidden = false
            c.facebook.isHidden = true
            
            c.photo.setShowActivityIndicator(true)
            c.photo.setIndicatorStyle(.gray)
            c.photo.sd_setImage(with: URL(string: "\(posts[indexPath.row].full_picture)"))
            
            c.photo.layer.frame.size.height = 200
            c.postTopMargin.constant = 240
        } else {
            c.photo.layer.frame.size.height = 50
            c.postTopMargin.constant = 90
            
            c.photo.isHidden = true
            c.facebook.isHidden = false
        }
        
        return c
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let url = URL(string: posts[indexPath.row].link)
        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
    }
    
    //
    // MARK: ExpandableLabel Delegate
    //
    
    func willExpandLabel(_ label: ExpandableLabel) {
        tableView.beginUpdates()
    }
    
    func didExpandLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView.indexPathForRow(at: point) as IndexPath? {
            states[indexPath.row] = false
            DispatchQueue.main.async { [weak self] in
                self?.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        tableView.endUpdates()
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
        tableView.beginUpdates()
    }
    
    func didCollapseLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView.indexPathForRow(at: point) as IndexPath? {
            states[indexPath.row] = true
            DispatchQueue.main.async { [weak self] in
                self?.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        tableView.endUpdates()
    }
    
    
}

