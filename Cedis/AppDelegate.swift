//
//  AppDelegate.swift
//  Cedis
//
//  Created by Administrator on 30/04/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications

var firebaseToken : String = ""


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        FirebaseApp.configure()
        let notificationTypes : UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
        let notificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
        application.registerForRemoteNotifications()
        application.registerUserNotificationSettings(notificationSettings)
        
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        
//        var launchedBefore = UserDefaults.standard.integer(forKey: "launchedBefore")
//        print(launchedBefore)
//        if launchedBefore == 0  {
//            //print("Vec je pokrenuto ranije.")
//
//            self.window = UIWindow(frame: UIScreen.main.bounds)
//
//            let storyboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//            let navigationController:UINavigationController = storyboard.instantiateViewController(withIdentifier: "navtut3") as! UINavigationController
//            let rootViewController:UIViewController = storyboard.instantiateViewController(withIdentifier: "drugi") as UIViewController
//            navigationController.viewControllers = [rootViewController]
//            self.window?.rootViewController = navigationController
//
//            self.window?.makeKeyAndVisible()
//
//        }
        
            
            
        
        
        
        
        return true
    }
    
    // Firebase notification received
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        
        let notificationType = UIApplication.shared.currentUserNotificationSettings?.types
        if notificationType?.rawValue != 0 {
            
            print("push enabled")
            // custom code to handle push while app is in the foreground
            //            print("Handle push from foreground\(notification.request.content.userInfo)")
            
            let dict = notification.request.content.userInfo["aps"] as! NSDictionary
            let d : [String : Any] = dict["alert"] as! [String : Any]
            var body : String = d["body"] as! String
            let title : String = d["title"] as! String
            print("Title:\(title) + body:\(body)")
            self.showAlertAppDelegate(title: title,message:body,buttonTitle:"ok",window:self.window!)
        } else {
            print("push disabled")
        }
    }
    
    
    func showAlertAppDelegate(title: String,message : String,buttonTitle: String,window: UIWindow){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: buttonTitle, style: UIAlertActionStyle.default, handler: nil))
        window.rootViewController?.present(alert, animated: false, completion: nil)
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print("MessageID : \(userInfo["gcm_message_id"])")
        
        print(userInfo)
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let defaults = UserDefaults.standard

        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            if (defaults.string(forKey: "token") != refreshedToken){
                defaults.set(refreshedToken, forKey: "token")
            }
        }
    }
    
    
//    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
//
//        Messaging.messaging().subscribe(toTopic: "/topics/cedis")
//    }
}
