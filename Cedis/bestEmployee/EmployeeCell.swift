//
//  EmployeeCell.swift
//  Cedis
//
//  Created by Administrator on 03/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit

class EmployeeCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        profileImage.layer.cornerRadius = 44.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var firstLastName: UILabel!
    @IBOutlet weak var sector: UILabel!
    @IBOutlet weak var workPlace: UILabel!
    @IBOutlet weak var badge: UIButton!
    

}
