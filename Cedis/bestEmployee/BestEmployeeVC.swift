//
//  BestEmployeeVC.swift
//  Cedis
//
//  Created by Administrator on 03/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

var bestEmployee = [BestEmployee]()
var nomineeList = [Sector]()
var candidateList = [Candidates]()

class BestEmployeeVC:BaseViewController,  UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var employeeTable: UITableView!
    
    @IBOutlet weak var navTitle: UINavigationItem!
    
    override func viewWillAppear(_ animated: Bool) {
        if (CheckConnection.isInternetAvailable() == true){
            getCandidates()
             }
        else{
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.employeeTable.delegate = self
        self.employeeTable.dataSource = self
        
        employeeTable.tableFooterView = UIView()
        self.employeeTable.separatorColor = UIColor.lightGray
    
    }
    
    func getCandidates() {
        
        bestEmployee = []
        nomineeList = []
        candidateList = []
        
        let url : String = "http://cedisklik.me/ws/_nomineeListIos.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSDictionary? {
                        
                        let votePeriodId = arraySchedule["votePeriodId"]! as? Int
                        let voteTitle = arraySchedule["voteTitle"]! as? String
                        let voteDateFrom = arraySchedule["voteDateFrom"]! as? String
                        let voteDateTo = arraySchedule["voteDateTo"]! as? String
                        
                        var objectNominee: NSDictionary?
                        
                        if let nominee = arraySchedule["nominee"]! as? NSArray{
                            var sectorId: Int?
                            var sectorName: String?
                            var objectCandidates: NSDictionary?
                            
                            for obj in nominee {
                                
                                objectNominee = obj as? NSDictionary
                                
                                sectorId = objectNominee!["sectorId"]! as? Int
                                sectorName = objectNominee!["sectorName"]! as? String
                                
                                if let candidates = objectNominee!["candidates"]! as? NSArray{
                                    var id: Int?
                                    var nameSurname: String?
                                    var profileImage: String?
                                    var workPlace: String?
                                    var isCurrentUserVoted: Bool?
                                    
                                    for objC in candidates {
                                        
                                        objectCandidates = objC as? NSDictionary
                                        
                                        id = objectCandidates!["id"]! as? Int
                                        nameSurname = objectCandidates!["nameSurname"]! as? String
                                        profileImage = objectCandidates!["profileImage"]! as? String
                                        workPlace = objectCandidates!["workPlace"]! as? String
                                        isCurrentUserVoted = objectCandidates!["isCurrentUserVoted"]! as? Bool
                                        
                                        candidateList.append(Candidates(id: id!, nameSurname: nameSurname!, profileImage: profileImage!, workPlace: workPlace!, isCurrentUserVoted: isCurrentUserVoted!))
                                        
                                    }
                                }
                                nomineeList.append(Sector(sectorId: sectorId!, sectorName: sectorName!, candidates: candidateList))
                                candidateList = []
                            }
                        }
                        bestEmployee.append(BestEmployee(votePeriodId: votePeriodId!, voteTitle: voteTitle!, voteDateFrom: voteDateFrom!, voteDateTo: voteDateTo!, nominee: nomineeList))
                        nomineeList = []
                        self.employeeTable.reloadData()
                        
                        let label = UILabel(frame: CGRect(x:0, y:0, width:400, height:50))
                        label.backgroundColor = .clear
                        label.numberOfLines = 2
                        label.font = UIFont(name:"Montserrat-Bold", size: 16.0)
                        label.textAlignment = .center
                        label.textColor = .white
                        
                        let title = bestEmployee[0].voteTitle
//                        var token = title.components(separatedBy: " - ")
                        label.text = "\("Glasanje za najboljeg radnika")\n\(title)"
                        self.navTitle.titleView = label
                        
                    }
                }
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        
        if(bestEmployee.count == 0){
            return 0
        }
        else{
           return bestEmployee[0].nominee.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bestEmployee[0].nominee[section].candidates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "EmployeeCell", for: indexPath) as! EmployeeCell
        
        //c.badge.tag = bestEmployee[0].nominee[indexPath.section].candidates[indexPath.row].id
        
        c.badge.tag = (indexPath.section * 1000) + indexPath.row
        
        c.profileImage.setShowActivityIndicator(true)
        c.profileImage.setIndicatorStyle(.gray)
        c.profileImage.sd_setImage(with: URL(string: "\(bestEmployee[0].nominee[indexPath.section].candidates[indexPath.row].profileImage)"))
        c.profileImage.layer.cornerRadius = 33
        c.firstLastName.text = bestEmployee[0].nominee[indexPath.section].candidates[indexPath.row].nameSurname
        c.workPlace.text = bestEmployee[0].nominee[indexPath.section].candidates[indexPath.row].workPlace
        c.sector.text = bestEmployee[0].nominee[indexPath.section].sectorName
        
        if(bestEmployee[0].nominee[indexPath.section].candidates[indexPath.row].isCurrentUserVoted == true){
            c.badge.setImage(UIImage(named: "like_red"), for: .normal)
        }
        else {
            c.badge.setImage(UIImage(named: "like_gray"), for: .normal)
        }
        
        c.badge.addTarget(self, action: #selector(self.vote(_:)), for: .touchUpInside)
        
        let separator = UIView(frame: CGRect(x: 0, y: 108, width:
            tableView.bounds.size.width, height: 5))
        separator.backgroundColor = UIColor(displayP3Red: 212/255, green: 53/255, blue: 53/255, alpha: 1)
        c.addSubview(separator)
        
        return c
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       return 55.0
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return bestEmployee[0].nominee[section].sectorName
//    }

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderTVC
        header.title.text = bestEmployee[0].nominee[section].sectorName
        
        return header
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    
    @objc func vote(_ sender: UIButton) {
        if (CheckConnection.isInternetAvailable() == true) {
            
            let row = sender.tag % 1000
            let section = sender.tag / 1000
            let candidateId = bestEmployee[0].nominee[section].candidates[row].id
            
            let votePeriod = bestEmployee[0].votePeriodId
            
            let url : String = "http://cedisklik.me/ws/_setVote.php?nomineeId=\(candidateId)&votePeriodId=\(votePeriod)"
            
            let headers = [
                "Authorization": "ApiKey \(userList[0].apiKey)",
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
                .validate()
                .responseJSON { (response) -> Void in
                    
                    if let requestData = response.result.value as! NSDictionary?{
                        
                        var code : String?
                        code = String(describing: requestData["error_code"]!)
                        if (code == "0" ){
                            if(sender.image(for: .normal) == UIImage(named: "like_gray") ){
                                sender.setImage(UIImage(named: "like_red") , for: UIControlState.normal)
                                bestEmployee[0].nominee[section].candidates[row].isCurrentUserVoted = true
                            }
                        } else{
                            let alert1 = UIAlertController(title: "Greška", message: "\(requestData["errors"]!)", preferredStyle: UIAlertControllerStyle.alert)
                            
                            alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  nil))
                            self.present(alert1, animated: true, completion: nil)
                        }
                    } else {
                        let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                        alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                        self.present(alert9, animated: true, completion: nil)
                    }
                    
                    
            }
        } else {
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
        }
    }
   
    
    
    
}
