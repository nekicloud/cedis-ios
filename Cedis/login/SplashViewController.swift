//
//  SplashViewController.swift
//  Cedis
//
//  Created by Administrator on 15/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire
import Lottie

class SplashViewController: BaseViewController {
    
    @IBOutlet weak var bckg: UIView!
    let defaults = UserDefaults.standard
    
    @IBOutlet weak var splash: UIImageView!
    
    override func viewDidAppear(_ animated: Bool) {
        
        bckg.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.newRED)
        bckg.transform = CGAffineTransform(translationX: 0, y: 1000)
        UIView.animate(withDuration: 1,
                       delay: 0.5,
                       animations: {
                        self.bckg.transform = CGAffineTransform(translationX: 0, y: 0)
        },
                       completion: { _ in
                        
                        let screenWidth = UIScreen.main.bounds.width
                        let screenHeight = UIScreen.main.bounds.height
                        
                        let superView = UIView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
                        superView.backgroundColor = UIColor.clear
                        self.bckg.addSubview(superView)
                        
                        let animationView = LOTAnimationView(name: "splash")
                        animationView.frame.size.width = screenWidth
                        animationView.frame.size.height = screenHeight
                        
                        animationView.contentMode = .scaleAspectFit
                        superView.addSubview(animationView)
                        
                        animationView.play{ (finished) in
                            if (self.defaults.string(forKey: "password") != nil && self.defaults.string(forKey: "password") != ""){
                                self.loginFunc(username: self.defaults.string(forKey: "username")!, password: self.defaults.string(forKey: "password")!)
                            } else {
                                self.performSegue(withIdentifier: "splashToLogin", sender: nil)
                            }
                        }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func loginFunc(username: String, password: String) {
        
        let parameters: Parameters = [
            "username": username,
            "password": password
        ]
        
        var error : String = ""
        let url : String = "http://cedisklik.me/ws/_login.php"
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let responseData = response.result.value as! NSDictionary? {
                    let errorCode : String? = String(describing: responseData["error_code"]!)
                    
                    if errorCode == "1" {
                        error = String(describing: responseData["errors"]!)
                        //                    self.errorLabel.isHidden = false
                        //                    self.errorLabel.text = error
                        self.performSegue(withIdentifier: "splashToLogin", sender: "test")
                    }
                    else if errorCode == "0" {
                        
                        //                    self.errorLabel.isHidden = true
                        //                    self.errorLabel.text = ""
                        if let userData = responseData["data"] as! NSDictionary? {
                            var loggedUser : User
                            
                            loggedUser = User(id: (userData["id"]! as? Int)!,
                                              nameSurname: (userData["nameSurname"]! as? String)!,
                                              jmbg: (userData["jmbg"]! as? String)!,
                                              email: (userData["email"]! as? String)!,
                                              phone: (userData["phone"]! as? String)!,
                                              profileImage: (userData["profileImage"]! as? String)!,
                                              workPlace: (userData["workPlace"]! as? String)!,
                                              role: (userData["role"]! as? Int)!,
                                              passwordChanged: (userData["role"]! as? Int)!,
                                              sector: (userData["sector"]! as? String)!,
                                              apiKey: (userData["apiKey"]! as? String)!,
                                              deviceId: (userData["deviceId"]! as? String)!,
                                              token: (userData["token"]! as? String)!)
                            userList.append(loggedUser)
                            self.performSegue(withIdentifier: "splashToMain", sender: "test")
                            
                        }
                    }
                    
                    //                    self.view.removeLoadingViews(animated: true)
                    //SwiftSpinner.hide()
                }
                else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert9, animated: true, completion: nil)
                }
        }
    }
    
    
}
