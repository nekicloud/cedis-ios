//
//  LoginVC.swift
//  Cedis
//
//  Created by Administrator on 30/04/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import CustomLoader
import Alamofire

var userList = [User]()
let defaults = UserDefaults.standard

class LoginVC:BaseViewController, UITextFieldDelegate {

//    override func viewWillAppear(_ animated: Bool) {
//        self.registerForKeyboardNotifications()
//        UIApplication.shared.statusBarStyle = .default
//
//    }
//    override func viewWillDisappear(_ animated: Bool) {
//        self.deregisterFromKeyboardNotifications()
//        UIApplication.shared.statusBarStyle = .lightContent
//    }
    

    
    var rememberMeValue = true
    
    @IBOutlet weak var showPassword: UIView!
    @IBOutlet weak var username: UITextField! {
        didSet {
            username?.addDoneToolbar(onDone: (target: self, action: #selector(doneButtonTappedForMyNumericTextField)))
        }
    }
    @objc func doneButtonTappedForMyNumericTextField() {
        username.resignFirstResponder()
    }
    
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var rememberMeView: RememberMe!
    @IBOutlet weak var logInBtn: UIButton!
    @IBOutlet weak var loginScroll: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.password.delegate = self;
        self.username.delegate = self;
        
        self.errorLabel.isHidden = true
        self.errorLabel.text = ""
        password.isSecureTextEntry = true
        self.logInBtn.layer.cornerRadius = 5.0
        
        
        //------show password click
        let gestureShowPass = UITapGestureRecognizer(target: self, action:  #selector (self.showPassAction))
        showPassword.isUserInteractionEnabled = true
        showPassword.addGestureRecognizer(gestureShowPass)
    }
    
    @IBAction func rememberMe(_ sender: Any) {
        
        if(rememberMeValue) {
            rememberMeValue = false
            rememberMeView.setImage(UIImage(named: "check.png"), for: .normal)
        } else {
            rememberMeValue = true
            rememberMeView.setImage(UIImage(named: "check_box.png"), for: .normal)
        }
        
    }
    
    @objc func showPassAction(_ sender:UITapGestureRecognizer){
        if(password.isSecureTextEntry) {
            password.isSecureTextEntry = false
        } else {
            password.isSecureTextEntry = true
        }
    }
    

    @IBAction func login(_ sender: Any) {
        
        if (CheckConnection.isInternetAvailable() == true){
            loginFunc()
        } else {
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
            
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true);
        return false;
    }
    
    
    
    func loginFunc() {
        
        let parameters: Parameters = [
            "username": username.text!,
            "password": password.text!
        ]
        
        var error : String = ""
        let url : String = "http://cedisklik.me/ws/_login.php"
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let responseData = response.result.value as! NSDictionary? {
                    let errorCode : String? = String(describing: responseData["error_code"]!)
                    
                    if errorCode == "1" {
                        error = String(describing: responseData["errors"]!)
                        self.errorLabel.isHidden = false
                        self.errorLabel.text = error
                    }
                    else if errorCode == "0" {
                        
                        self.errorLabel.isHidden = true
                        self.errorLabel.text = ""

                        if let userData = responseData["data"] as! NSDictionary? {
                            var loggedUser : User

                            loggedUser = User(id: (userData["id"]! as? Int)!,
                                              nameSurname: (userData["nameSurname"]! as? String)!,
                                              jmbg: (userData["jmbg"]! as? String)!,
                                              email: (userData["email"]! as? String)!,
                                              phone: (userData["phone"]! as? String)!,
                                              profileImage: (userData["profileImage"]! as? String)!,
                                              workPlace: (userData["workPlace"]! as? String)!,
                                              role: (userData["role"]! as? Int)!,
                                              passwordChanged: (userData["passwordChanged"]! as? Int)!,
                                              sector: (userData["sector"]! as? String)!,
                                              apiKey: (userData["apiKey"]! as? String)!,
                                              deviceId: (userData["deviceId"]! as? String)!,
                                              token: (userData["token"]! as? String)!)
                            userList.append(loggedUser)
                        
                            if (self.rememberMeValue) {
                                defaults.set(self.username.text!, forKey: "username")
                                defaults.set(self.password.text!, forKey: "password")
                            }
                            
                            if(loggedUser.passwordChanged == 0) {
                                self.performSegue(withIdentifier: "changePassword", sender: "test")
                            } else {
                                self.performSegue(withIdentifier: "loginToMain", sender: "test")
                            }
                        }
                    }
                }
                else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert9, animated: true, completion: nil)
                }
                
        }
       
    }
    
    
//    @objc override func keyboardWasShown(notification: NSNotification){
//
//        if let infoKey  = notification.userInfo?[UIKeyboardFrameEndUserInfoKey],
//            let rawFrame = (infoKey as AnyObject).cgRectValue {
//            let keyboardFrame = view.convert(rawFrame, from: nil)
//            loginScroll.contentInset = UIEdgeInsetsMake(0, 0, keyboardFrame.height, 0)
//            let bottomOffset = CGPoint(x: 0, y: keyboardFrame.height)
//            loginScroll.setContentOffset(bottomOffset, animated: true)
//        }
//    }
//
//    @objc override func keyboardWillBeHidden(notification: NSNotification){
//        //Once keyboard disappears, restore original positions
//        self.view.endEditing(true)
//        loginScroll.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
//    }

}

extension UITextField {
    func addDoneToolbar(onDone: (target: Any, action: Selector)? = nil, onCancel: (target: Any, action: Selector)? = nil) {
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))
        
        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Return", style: .done, target: onDone.target, action: onDone.action)
        ]
        toolbar.sizeToFit()
        
        self.inputAccessoryView = toolbar
    }
    
    // Default actions:
    @objc func doneButtonTapped() {
        self.resignFirstResponder()
    }
}

