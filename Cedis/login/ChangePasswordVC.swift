//
//  ChangePassVC.swift
//  Cedis
//
//  Created by Administrator on 24/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire

class ChangePasswordVC:BaseViewController, UITextFieldDelegate {

    @IBOutlet weak var toolbar: UIView!
    @IBOutlet weak var back: UIView!
    @IBOutlet weak var showOldPass: UIView!
    @IBOutlet weak var showNewPass: UIView!
    @IBOutlet weak var showNewPassAgain: UIView!
        
    @objc func backAction(_ sender:UITapGestureRecognizer){
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            self.dismiss(animated: true, completion: nil)
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //------back click
        let gestureBack = UITapGestureRecognizer(target: self, action:  #selector (self.backAction))
        back.isUserInteractionEnabled = true
        back.addGestureRecognizer(gestureBack)
        
        
        oldPass.delegate = self
        newPass.delegate = self
        rePass.delegate = self
        
        oldPass.isSecureTextEntry = true
        newPass.isSecureTextEntry = true
        rePass.isSecureTextEntry = true
        
        oldPass.layer.borderWidth = 1
        oldPass.layer.borderColor = UIColor.lightGray.cgColor
        oldPass.layer.cornerRadius = 5
        
        newPass.layer.borderWidth = 1
        newPass.layer.borderColor = UIColor.lightGray.cgColor
        newPass.layer.cornerRadius = 5
        
        rePass.layer.borderWidth = 1
        rePass.layer.borderColor = UIColor.lightGray.cgColor
        rePass.layer.cornerRadius = 5
        
        savePass.layer.cornerRadius = 5
        
        
        //------show password click
        let gestureShowOldPass = UITapGestureRecognizer(target: self, action:  #selector (self.showOldPassAction))
        showOldPass.isUserInteractionEnabled = true
        showOldPass.addGestureRecognizer(gestureShowOldPass)
        
        
        //------show password click
        let gestureShowNewPass = UITapGestureRecognizer(target: self, action:  #selector (self.showNewPassAction))
        showNewPass.isUserInteractionEnabled = true
        showNewPass.addGestureRecognizer(gestureShowNewPass)
        
        
        //------show password click
        let gestureShowNewPassAgain = UITapGestureRecognizer(target: self, action:  #selector (self.showNewPassAgainAction))
        showNewPassAgain.isUserInteractionEnabled = true
        showNewPassAgain.addGestureRecognizer(gestureShowNewPassAgain)
        
    }
    
    
    @objc func showOldPassAction(_ sender:UITapGestureRecognizer){
        if(oldPass.isSecureTextEntry) {
            oldPass.isSecureTextEntry = false
        } else {
            oldPass.isSecureTextEntry = true
        }
    }
    
    @objc func showNewPassAction(_ sender:UITapGestureRecognizer){
        if(newPass.isSecureTextEntry) {
            newPass.isSecureTextEntry = false
        } else {
            newPass.isSecureTextEntry = true
        }
    }
    
    
    @objc func showNewPassAgainAction(_ sender:UITapGestureRecognizer){
        if(rePass.isSecureTextEntry) {
            rePass.isSecureTextEntry = false
        } else {
            rePass.isSecureTextEntry = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
//
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .lightContent
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true);
        return false;
    }

    @IBOutlet weak var oldPass: UITextField!
    @IBOutlet weak var newPass: UITextField!
    @IBOutlet weak var rePass: UITextField!
    @IBOutlet weak var savePass: UIButton!
    
    @IBAction func changePass(_ sender: Any) {
        
        let url : String = "http://cedisklik.me/ws/_changePassword.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        let oldPassString = oldPass.text
        let newPassString = newPass.text
        let rePassString = rePass.text
        
        let parameters1: Parameters = [
            "oldPassword": oldPassString!,
            "newPassword": newPassString!,
            "reNewPassword": rePassString!
        ]
        
        Alamofire.request(url, method: .post, parameters: parameters1, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    var code : String?
                    
                    code = String(describing: requestData["error_code"]!)
                    
                    if code == "1" {
                        
                        let error = String(describing: requestData["errors"]!)
                        
                        let alert9 = UIAlertController(title: "Obavještenje", message: "\(error)", preferredStyle: UIAlertControllerStyle.alert)
                        alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
                        self.present(alert9, animated: true, completion: nil)
                        
                    } else {
                        defaults.set(newPassString, forKey: "Password")

                        var data : String?
                        data = String(describing: requestData["data"]!)
                        
                        let alert9 = UIAlertController(title: "Obavještenje", message: "\(data!)", preferredStyle: UIAlertControllerStyle.alert)
                        alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { action in
                            self.performSegue(withIdentifier: "passToMain", sender: "test")
                        }))
                        self.present(alert9, animated: true, completion: nil)
                    }
                }
                else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert9, animated: true, completion: nil)
                }
                
                
        }
        
    }
    
}
