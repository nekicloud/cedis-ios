//
//  ContactHRVC.swift
//  Cedis
//
//  Created by Administrator on 18/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire

class ContactHRVC:BaseViewController, UITextViewDelegate, UITextFieldDelegate {

    override func viewWillAppear(_ animated: Bool) {
        self.registerForKeyboardNotifications()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.deregisterFromKeyboardNotifications()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        message.delegate = self
        messageTitle.delegate = self
        
        message.layer.borderWidth = 0.8
        message.layer.borderColor = UIColor.lightGray.cgColor
        message.layer.cornerRadius = 5.0
        
        messageTitle.layer.borderWidth = 0.8
        messageTitle.layer.borderColor = UIColor.lightGray.cgColor
        messageTitle.layer.cornerRadius = 5.0
        
        sendBtn.layer.cornerRadius = 5.0
        
        
    }

    @IBOutlet weak var messageTitle: UITextView!
    @IBOutlet weak var message: UITextView!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var scrollContact: UIScrollView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true);
        return false;
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    @objc override func keyboardWasShown(notification: NSNotification){
        
        if let infoKey  = notification.userInfo?[UIKeyboardFrameEndUserInfoKey],
            let rawFrame = (infoKey as AnyObject).cgRectValue {
            let keyboardFrame = view.convert(rawFrame, from: nil)
            scrollContact.contentInset = UIEdgeInsetsMake(0, 0, keyboardFrame.height / 2 + 70, 0)
        }
    }
    
    @objc override func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        self.view.endEditing(true)
        scrollContact.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        
    }
    
    /* Older versions of Swift */
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func sendMessage(_ sender: Any) {
        
        let messageTitleString = self.messageTitle.text
        let messageString = self.message.text
        
        let parameters: Parameters = [
            "title": messageTitleString!,
            "message": messageString!
        ]
        
        let url : String = "http://cedisklik.me/ws/_contactHR.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        var error : String = ""
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    var code : String?
                    code = String(describing: requestData["error_code"]!)
                    
                    if code == "1" {
                        
                        let alert1 = UIAlertController(title: "Obavještenje", message: "\(requestData["errors"]!)", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  { action in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert1, animated: true, completion: nil)
                        
                    }
                    else {
                        
                        var data : String?
                        data = String(describing: requestData["data"]!)
                        
                        let alert1 = UIAlertController(title: "Obavještenje", message: "\(data!)", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  { action in
                            self.navigationController?.popViewController(animated: true)
                            self.messageTitle.text = ""
                            self.message.text = ""
                            self.performSegue(withIdentifier: "showMssgDetails", sender: self)
                        }))
                        self.present(alert1, animated: true, completion: nil)
                    }
                }
                else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert9, animated: true, completion: nil)
                }
                
        }
        
    }
    
    
}
