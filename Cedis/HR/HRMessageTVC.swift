//
//  ConversationMessagesCell.swift
//  Cedis
//
//  Created by Administrator on 18/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit

class HRMessageTVC: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    @IBOutlet weak var messageTop: NSLayoutConstraint!
    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var timestamp: UILabel!
    @IBOutlet weak var messageText: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
