//
//  RememberMe.swift
//  Cedis
//
//  Created by Administrator on 15/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit

class RememberMe: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 10, left: 40, bottom: 10, right: (bounds.width - 60))
            titleEdgeInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        }
    }

}
