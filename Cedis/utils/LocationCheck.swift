//
//  LocationCheck.swift
//  Cedis
//
//  Created by Administrator on 18/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import Foundation
import UIKit


class LocationCheck: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: (bounds.width - 20))
            titleEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        }
    }
    
}
