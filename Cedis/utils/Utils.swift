//
//  Utils.swift
//  Cedis
//
//  Created by Administrator on 15/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import Foundation
import UIKit


struct User {
    var id: Int
    var nameSurname: String
    var jmbg: String
    var email: String
    var phone: String
    var profileImage: String
    var workPlace: String
    var role: Int
    var passwordChanged: Int
    var sector: String
    var apiKey: String
    var deviceId: String
    var token: String
}

struct News {
    var id: Int
    var title: String
    var content: String
    var image: String
    var publishDate: String
    var documents: [Documents]
}

struct FacebookPost {
    var link: String
    var message: String
    var full_picture: String
    var id: String
}

struct InstagramPost {
    var created_time: String
    var caption: String
    var type: String
    var link: String
    var photoUrl: String
}

struct Documents {
    var documentId: Int
    var documentPath: String
}

struct VacationTypes {
    var id: Int
    var name: String
}

struct ProblemTypes {
    var id: Int
    var name: String
}

struct Vacation {
    var id: Int
    var dateFrom: String
    var dateTo: String
    var typeName: String
    var comment: String
    var statusId: Int
    var statusName: String
    var requestDate: String
}

struct Confirmation {
    
    var id: Int
    var vrsta_potvrde: String
    var broj_mjeseci: String
    var namjena_potvrde: String
    var statusId: Int
    var vrijeme: String
    var ime_banke: String
    var sjediste_banke: String
    var broj_rata: String
    var ime_drustva: String
    var sjediste_drustva: String
    var ostalo: String
}

struct BestEmployee {
    var votePeriodId: Int
    var voteTitle: String
    var voteDateFrom: String
    var voteDateTo: String
    var nominee: [Sector]
}

struct Sector {
    var sectorId: Int
    var sectorName: String
    var candidates: [Candidates]
}

struct Candidates {
    var id: Int
    var nameSurname: String
    var profileImage: String
    var workPlace: String
    var isCurrentUserVoted: Bool
}


struct Constants {
    
    struct Colors {
        static let background = "#2D3741"

        static let red = "#C72028"
        static let newRED = "CF0304"
        static let grey = "#c5c4c4"  // nova boja 5d5d5d
        static let green = "#6da337"
        static let dark = "#1e303c"
        static let divider = "ededed"
        
        static let white = "#FFFFFF"
        
    }
    
}

struct Conversations {
    var conversationId: Int
    var title: String
    var timestamp: String
    var status: Int
    var archived: Int
    var messages: [Messages]
}


struct Payroll {
    var link: String
    var year: Int
    var month: String
}

struct Messages {
    var id: Int
    var message: String
    var messageType: Int
    var timestamp: String
}

struct ActCategory {
    var opened : Bool
    var id: Int
    var name: String
    var icon: String
    var documents: [ActDocument]
}

struct ActDocument {
    var id: Int
    var title: String
    var filePath: String
}

struct SurveyQuestionOption {
    
    var questionOptionID : Int
    var questionOptionValue : String
}

struct SurveySelectedOption {
    
    var questionId : Int
    var optionId : Int
}

struct SurveyQuestion {
    
    var questionID : Int
    var questionTitle : String
    var questionOptions : [SurveyQuestionOption]
    var selectedOption : Int;

}

struct Survey {
    var surveyID : Int
    var surveyTitle : String
    var surveyCreationDate : String
    var surveyStatus : Int
    var questions : [SurveyQuestion]
    
}

struct Comment {
    
    var userID: Int
    var commentID: Int
    var username: String
    var imagePath: String
    var commentText: String
    var creationDate: String
}

struct Like {
    
    var username: String
    var imagePath: String
}

class Utils {
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat(Float((rgbValue & 0xFF0000) >> 16) / 255.0),
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func formatName(fullName:String) -> String {
        
        let smallcaps = fullName.lowercased() as NSString
        return smallcaps.capitalized(with: NSLocale.current)
    }
}
