//
//  MoreVC.swift
//  Cedis
//
//  Created by Administrator on 17/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import Firebase

class HomeVC: BaseViewController, UIScrollViewDelegate {
    
    var newsList = [News]()
    var documentsList = [Documents]()
    let defaults = UserDefaults.standard

    var timer = Timer()
    var nextPage : Int = 1
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var myProfile: UIView!
    @IBOutlet weak var problem: UIView!
    @IBOutlet weak var sendRequest: UIView!
    @IBOutlet weak var gallery: UIView!
    @IBOutlet weak var payrolls: UIView!
    @IBOutlet weak var confirmations: UIView!
    @IBOutlet weak var internalActs: UIView!
    @IBOutlet weak var surveys: UIView!
    @IBOutlet weak var socialNetworks: UIView!

    func sendToken(){
        
        let url : String = "http://cedisklik.me/ws/_saveToken.php?device_id=\(UIDevice.current.identifierForVendor!.uuidString)&token=\(self.defaults.string(forKey: "token")!)"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    let data = requestData["data"] as! String
                    
//                    if (data == "Success.") {
//                        self.defaults.set(false, forKey: "shouldUpdateToken")
//                    }
                }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = Utils().hexStringToUIColor(hex: "F4F4F4");
        
        Messaging.messaging().subscribe(toTopic: "/topics/news")
        if (self.defaults.string(forKey: "token") != nil) {
            sendToken()
        }

        getNews()
        
        scrollView.isHidden = true
        scrollView.delegate = self
        
        myProfile.layer.cornerRadius = 5.0
        problem.layer.cornerRadius = 5.0
        sendRequest.layer.cornerRadius = 5.0
        payrolls.layer.cornerRadius = 5.0
        internalActs.layer.cornerRadius = 5.0
        confirmations.layer.cornerRadius = 5.0
        surveys.layer.cornerRadius = 5.0
        socialNetworks.layer.cornerRadius = 5.0
        gallery.layer.cornerRadius = 5.0
        
        payrolls.alpha = 0.3
        
        let gestureMyProfile = UITapGestureRecognizer(target: self, action:  #selector (self.openProfile (_:)))
        let gestureProblem = UITapGestureRecognizer(target: self, action:  #selector (self.openProblem (_:)))
        let gestureContacts = UITapGestureRecognizer(target: self, action:  #selector (self.openContacts (_:)))
        let gesturePayroll = UITapGestureRecognizer(target: self, action:  #selector (self.openPayroll (_:)))
        let gestureInternalActs = UITapGestureRecognizer(target: self, action:  #selector (self.openInternalActs (_:)))
        let gestureConfirmations = UITapGestureRecognizer(target: self, action:  #selector (self.openConfirmations (_:)))
        let gestureSurveys = UITapGestureRecognizer(target: self, action:  #selector (self.openSurveys (_:)))
        let gestureSocialNetworks = UITapGestureRecognizer(target: self, action:  #selector (self.openSocialNetworks (_:)))
        let gestureGallery = UITapGestureRecognizer(target: self, action:  #selector (self.openGallery (_:)))
        
        self.myProfile.addGestureRecognizer(gestureMyProfile)
        self.problem.addGestureRecognizer(gestureProblem)
        self.sendRequest.addGestureRecognizer(gestureContacts)
        self.payrolls.addGestureRecognizer(gesturePayroll)
        self.internalActs.addGestureRecognizer(gestureInternalActs)
        self.confirmations.addGestureRecognizer(gestureConfirmations)
        self.surveys.addGestureRecognizer(gestureSurveys)
        self.socialNetworks.addGestureRecognizer(gestureSocialNetworks)
        self.gallery.addGestureRecognizer(gestureGallery)
        
    }
    
    @objc func openProfile(_ sender:UITapGestureRecognizer){
        let storyboard: UIStoryboard = UIStoryboard(name: "MyProfile", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "myProfile") as! MyProfileVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func openProblem(_ sender:UITapGestureRecognizer){
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Problem", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "reportProblem") as! ReportProblemVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func openContacts(_ sender:UITapGestureRecognizer){
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Requests", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "requests") as! RequestForVacationVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    @objc func openPayroll(_ sender:UITapGestureRecognizer){
//        performSegue(withIdentifier: "payrollSegue", sender: nil)
    }
    @objc func openInternalActs(_ sender:UITapGestureRecognizer){
        performSegue(withIdentifier: "actSegue", sender: nil)
    }
    @objc func openConfirmations(_ sender:UITapGestureRecognizer){
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Confirmations", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "confirmations") as! NewConfirmationVC
        //        self.present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    
    }
    @objc func openSurveys(_ sender:UITapGestureRecognizer){
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Surveys", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "surveyList") as! SurveyListVC
        //        self.present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @objc func openSocialNetworks(_ sender:UITapGestureRecognizer){
        let storyboard: UIStoryboard = UIStoryboard(name: "Socials", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "socials") as! SocialNetworksVC
        //        self.present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func openGallery(_ sender:UITapGestureRecognizer){
        let storyboard: UIStoryboard = UIStoryboard(name: "Gallery", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "gallery") as! GalleryVC
//        self.present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.tabBarController?.tabBar.isHidden = false
        
        let selectedColor   = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        let normalColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.4)
        
        self.tabBarController?.tabBar.tintColor = selectedColor
        self.tabBarController?.tabBar.unselectedItemTintColor = normalColor
        
        let selectedAttributes = [NSAttributedStringKey.font:
            UIFont(name: "Montserrat-Regular", size: 8.0)!,
                                  NSAttributedStringKey.foregroundColor: selectedColor] as! [NSAttributedStringKey: Any]
        
        let normalAttributes = [NSAttributedStringKey.font:
            UIFont(name: "Montserrat-Regular", size: 8.0)!,
                                NSAttributedStringKey.foregroundColor: normalColor] as! [NSAttributedStringKey: Any]
        
        UITabBarItem.appearance().setTitleTextAttributes(normalAttributes, for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes(selectedAttributes, for: .selected)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    var pageWidth = 0
    
    fileprivate func setupScroller() {
        
        var scrollContent : CGFloat = 0
        
        for i in 0...newsList.count - 1 {
            
            let holder = UIView(frame: CGRect(x:scrollContent, y:0, width:scrollView.frame.width, height:scrollView.frame.height))
            
            //on iphone 5s it's 89
            if(holder.frame.size.height > 90) {
                
                let img = UIImageView(frame: CGRect(x:10, y:10, width:holder.frame.width - 20, height:holder.frame.height - 60))
                img.sd_setImage(with: URL(string: "\(newsList[i].image.replacingOccurrences(of: " ", with: "%20"))"))
                img.contentMode = .scaleAspectFill
                img.layer.cornerRadius = 5
                img.clipsToBounds = true
                holder.addSubview(img)
                
                let label = UILabel(frame: CGRect(x: 10, y: holder.frame.height - 50, width: holder.frame.width - 20, height: 50))
                label.textAlignment = .left
                label.textColor = Utils().hexStringToUIColor(hex: Constants.Colors.dark)
                label.font = UIFont(name: "Montserrat-Regular", size: 15.0)
                label.text = newsList[i].title
                label.lineBreakMode = .byTruncatingTail
                label.numberOfLines = 2
                holder.addSubview(label)
                
            } else {
                
                let img = UIImageView(frame: CGRect(x:10, y:10, width: 70, height:holder.frame.height - 20))
                img.sd_setImage(with: URL(string: "\(newsList[i].image.replacingOccurrences(of: " ", with: "%20"))"))
                img.contentMode = .scaleAspectFill
                img.layer.cornerRadius = 5
                img.clipsToBounds = true
                holder.addSubview(img)

                let label = UILabel(frame: CGRect(x: 90, y: 10, width: holder.frame.width - 100, height:holder.frame.height - 20))
                label.textAlignment = .left
                label.textColor = Utils().hexStringToUIColor(hex: Constants.Colors.dark)
                label.font = UIFont(name: "Montserrat-Regular", size: 15.0)
                label.text = newsList[i].title
                label.lineBreakMode = .byTruncatingTail
                label.numberOfLines = 3
                holder.addSubview(label)
            }
            
            holder.layer.cornerRadius = 5
            holder.layer.borderColor = Utils().hexStringToUIColor(hex: Constants.Colors.grey).cgColor
            holder.layer.borderWidth = 0.5
            holder.clipsToBounds = true
            
            holder.isUserInteractionEnabled = true
            
            holder.backgroundColor = .white
            holder.tag = i
            
            //------scroller click_
            let gestureNews = UITapGestureRecognizer(target: self, action:  #selector (self.openNews))
            holder.isUserInteractionEnabled = true
            holder.addGestureRecognizer(gestureNews)
            
            self.scrollView.addSubview(holder)
            let divider = UIView(frame: CGRect(x:scrollContent, y:0, width:10, height:scrollView.frame.height))
            self.scrollView.addSubview(divider)
            
            pageWidth = Int(holder.frame.width)
            scrollContent = scrollContent + CGFloat(pageWidth)
        }
        
        scrollView.contentSize = CGSize(width: scrollContent, height: scrollView.frame.height)
        runTimer()
    }
    
    
    @objc func openNews(_ sender:UITapGestureRecognizer){
        let storyboard: UIStoryboard = UIStoryboard(name: "News", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "newsContent") as! NewsDetails
        let v = sender.view!
        let tag = v.tag
        vc.news = newsList[tag]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getNews(){
        
        
        let url : String = "http://cedisklik.me/ws/_getPinnedNews.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSArray? {
                        
                        var id: Int?
                        var title: String?
                        var content: String?
                        var image: String?
                        var publishDate: String?
                        
                        var object: NSDictionary?
                        var objectDoc: NSDictionary?
                        
                        for objSchedule in arraySchedule {
                            
                            object = objSchedule as? NSDictionary
                            
                            id = object!["id"]! as? Int
                            title = object!["title"]! as? String
                            content = object!["content"]! as? String
                            image = object!["image"]! as? String
                            publishDate = object!["publishDate"]! as? String
                            
                            if let documents = object!["documents"]! as? NSArray{
                                var documentId: Int?
                                var documentPath: String?
                                
                                for obj in documents {
                                    
                                    objectDoc = obj as? NSDictionary
                                    
                                    documentId = objectDoc!["documentId"]! as? Int
                                    documentPath = objectDoc!["documentPath"]! as? String
                                    
                                    self.documentsList.append(Documents(documentId: documentId!, documentPath: documentPath!))
                                }
                            }
                            
                            self.newsList.append(News(id: id!, title: title!, content: content!, image: image!, publishDate: publishDate!, documents: self.documentsList))
                            
                            self.documentsList = []
                        }
                    }

                    self.scrollView.isHidden = false
                    self.setupScroller()

                }
        }
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 3, target: self,   selector: (#selector(updateScroller)), userInfo: nil, repeats: true)
    }
    
    @objc func updateScroller() {
        if(nextPage < newsList.count) {
            let x : CGFloat = CGFloat(nextPage) * CGFloat(pageWidth)
            scrollView.setContentOffset(CGPoint(x: x, y: 0), animated: true)
            
            nextPage = nextPage + 1
        } else {
            timer.invalidate()
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.isEqual(scrollView) {
            switch scrollView.panGestureRecognizer.state {
            case .began:
                timer.invalidate()
                print("began")
//            case .changed:
                // User is currently dragging the scroll view
//                print("changed")
//            case .possible:
                // The scroll view scrolling but the user is no longer touching the scrollview (table is decelerating)
//                print("possible")
            default:
                break
            }
        }
    }
}
