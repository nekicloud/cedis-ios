//
//  ShowDocumentVC.swift
//  Cedis
//
//  Created by Administrator on 15/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import CustomLoader

class ShowDocumentVC:BaseViewController, UIWebViewDelegate {
    
    var documentTitle : String = ""
    var documentUrl : NSString = ""
    
    @IBOutlet weak var navItemTitle: UINavigationItem!
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func webViewDidStartLoad(_ webView: UIWebView){
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView){
        self.view.removeLoadingViews(animated: true)
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LoadingView.standardProgressBox.show(inView: view)
        self.webView.delegate = self
        
        navItemTitle.title = documentTitle
        
        if (CheckConnection.isInternetAvailable() == true){
            //            let urlString = documentUrl//.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            let urlStr : NSString = documentUrl.addingPercentEscapes(using: String.Encoding.utf8.rawValue)! as NSString
            let myURL = URL(string: urlStr as String)
            let myRequest = URLRequest(url: myURL!)
            webView.loadRequest(myRequest)
//            webView.scalesPageToFit=YES
        } else {
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    @IBOutlet weak var webView: UIWebView!
    
}
