//
//  FooterMenu.swift
//  Cedis
//
//  Created by Administrator on 30/04/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit

class FooterMenu: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vc1 = UIStoryboard(name: "News", bundle: nil).instantiateViewController(withIdentifier: "news") as? NewsVC
        let navController1 = UINavigationController(rootViewController: vc1!)
        navController1.tabBarItem.title = "Aktuelnosti"
        navController1.tabBarItem.image = imageWithImage(image: UIImage(named:"ic_news")!, scaledToSize:CGSize(width: 30, height: 30))
        
//        let vc2 = UIStoryboard(name: "Voting", bundle: nil).instantiateViewController(withIdentifier: "vote") as? BestEmployeeVC
//        let navController2 = UINavigationController(rootViewController: vc2!)
//        navController2.tabBarItem.title = "Radnik mjeseca"
//        navController2.tabBarItem.image = imageWithImage(image: UIImage(named:"ic_employees")!, scaledToSize:CGSize(width: 30, height: 30))
        
        let vc3 = UIStoryboard(name: "ContactHR", bundle: nil).instantiateViewController(withIdentifier: "contactHR") as? ContactHRVC
        let navController3 = UINavigationController(rootViewController: vc3!)
        navController3.tabBarItem.title = "Kontaktiraj HR"
        navController3.tabBarItem.image = imageWithImage(image: UIImage(named:"ic_contact")!, scaledToSize:CGSize(width: 30, height: 30))
    
        let vc4 = UIStoryboard(name: "Contacts", bundle: nil).instantiateViewController(withIdentifier: "contacts") as? ContactsVC
        let navController4 = UINavigationController(rootViewController: vc4!)
        navController4.tabBarItem.title = "Imenik"
        navController4.tabBarItem.image = imageWithImage(image: UIImage(named:"imenik")!, scaledToSize:CGSize(width: 30, height: 30))
        
        let vc5 = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "more") as? HomeVC
        let navController5 = UINavigationController(rootViewController: vc5!)
        navController5.tabBarItem.title = "Početna"
        navController5.tabBarItem.image = imageWithImage(image: UIImage(named:"ic_home")!, scaledToSize:CGSize(width: 30, height: 30))

        self.viewControllers = [navController5, navController1, navController3, navController4]
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let selectedColor   = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
        let normalColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.4)
        
        self.tabBar.tintColor = selectedColor
        self.tabBar.unselectedItemTintColor = normalColor
        
        let selectedAttributes = [NSAttributedStringKey.font:
            UIFont(name: "Montserrat-Regular", size: 14.0)!,
                                  NSAttributedStringKey.foregroundColor: selectedColor] as! [NSAttributedStringKey: Any]
        
        let normalAttributes = [NSAttributedStringKey.font:
            UIFont(name: "Montserrat-Regular", size: 14.0)!,
                                NSAttributedStringKey.foregroundColor: normalColor] as! [NSAttributedStringKey: Any]
        
        UITabBarItem.appearance().setTitleTextAttributes(normalAttributes, for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes(selectedAttributes, for: .selected)
    }
    
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: newSize.width, height: newSize.height)))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}
