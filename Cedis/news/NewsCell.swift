//
//  NewsCell.swift
//  Cedis
//
//  Created by Administrator on 03/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var newsImage: UIImageView!
    @IBOutlet weak var newsDate: UILabel!
    @IBOutlet weak var newsTitle: UILabel!
    

}
