//
//  NewsDetails.swift
//  Cedis
//
//  Created by Administrator on 03/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

var indDoc: Int?

class NewsDetails:BaseViewController, UIWebViewDelegate {
    
    var news : News?
    
    @IBOutlet weak var fabDocument: UIButton!
    @IBOutlet weak var imagePath: UIImageView!
    @IBOutlet weak var publishDate: UILabel!
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var content: NSLayoutConstraint!
    
    @IBOutlet weak var myWebViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        fabDocument.layer.cornerRadius = 24.0
        
        imagePath.setShowActivityIndicator(true)
        imagePath.setIndicatorStyle(.gray)
        imagePath.sd_setImage(with: URL(string: "\(news!.image.replacingOccurrences(of: " ", with: "%20"))"))

        newsTitle.text = news!.title
        publishDate.text = news!.publishDate
        
        webView.delegate = self
        
//        let content = "<span style=\"font-family: 'Roboto';" +
//            "font-size: 16px;" + "text-align:justify;\">" +
//            news!.content + " </span>"
        
       let content = String(format: "<html><body><span style=\"font-family:%@;text-align:justify\">%@</span></body></html>", "Roboto", news!.content)
        webView.loadHTMLString(content, baseURL: nil)
        webView.scrollView.isScrollEnabled = false
        
        if (news!.documents.isEmpty){
            fabDocument.isHidden = true
        } else if(news!.documents[0].documentPath.hasSuffix(".docx") || news!.documents[0].documentPath.hasSuffix(".doc")) {
            fabDocument.isHidden = false
            fabDocument.setImage(UIImage(named: "ic_doc"), for: .normal)
        } else if(news!.documents[0].documentPath.hasSuffix(".pdf") == true) {
            fabDocument.isHidden = false
            fabDocument.setImage(UIImage(named: "ic_pdf small"), for: .normal)
        }
        
        readNews(id: news!.id)
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(.zero)
        webView.scrollView.isScrollEnabled=false;
        myWebViewHeightConstraint.constant = webView.scrollView.contentSize.height
        content.constant = 400 + webView.scrollView.contentSize.height
        webView.scalesPageToFit = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: false)

    }
    
    @IBAction func openNewsDoc(_ sender: Any) {
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "webVC") as! ShowDocumentVC
        
        vc.documentTitle = news!.title
        vc.documentUrl = news!.documents[0].documentPath as NSString
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func readNews(id: Int){
        
        
        let url : String = "http://cedisklik.me/ws/_readNewsByID.php?newsID=\(id)"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSString? {
                        print(arraySchedule)
                    }
                }
        }
    }
    
    
    
}
