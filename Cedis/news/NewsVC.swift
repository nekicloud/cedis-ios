//
//  NewsVC.swift
//  Cedis
//
//  Created by Administrator on 03/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage



class NewsVC:BaseViewController,  UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {

    let searchBar = UISearchBar()
    var searchNews : Bool = false
    
    var newsSelection: Int?
    
    var newsList = [News]()
    var documentsList = [Documents]()
    
    var tempList = [News]()

    @IBOutlet weak var searchHolder: UIView!
    @IBOutlet weak var newsTable: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        if (CheckConnection.isInternetAvailable() == true){
            getNews()
        }
        else{
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
        }
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        //        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.barTintColor = Utils().hexStringToUIColor(hex:Constants.Colors.red)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        searchBar.placeholder = "Pretraži"
        searchBar.frame = CGRect(x: 0, y: 0, width: (view.frame.size.width), height: 64)
        searchBar.barStyle = .default
        searchBar.isTranslucent = false
        searchBar.barTintColor = Utils().hexStringToUIColor(hex:Constants.Colors.red)
        searchBar.backgroundImage = UIImage()
        searchBar.enablesReturnKeyAutomatically = false
        searchBar.tintColor = .black

        searchBar.delegate = self
        searchHolder.addSubview(searchBar)
        searchBar.returnKeyType = UIReturnKeyType.done
        
        topLayoutGuide.bottomAnchor.constraint(equalTo: searchBar.topAnchor).isActive = true
        
        
        newsTable.delegate = self
        newsTable.dataSource = self
        
        newsTable.tableFooterView = UIView()
        newsTable.separatorColor = UIColor.clear
        newsTable.backgroundColor = Utils().hexStringToUIColor(hex:Constants.Colors.red)
        
    }
    
    func getNews(){
        
        newsList = []
        documentsList = []
        
        let url : String = "http://cedisklik.me/ws/_getBulletinBoard.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSArray? {
                        
                        var id: Int?
                        var title: String?
                        var content: String?
                        var image: String?
                        var publishDate: String?
                        
                        var object: NSDictionary?
                        var objectDoc: NSDictionary?
                        
                        for objSchedule in arraySchedule {
                            
                            object = objSchedule as? NSDictionary
                            
                            id = object!["id"]! as? Int
                            title = object!["title"]! as? String
                            content = object!["content"]! as? String
                            image = object!["image"]! as? String
                            publishDate = object!["publishDate"]! as? String
                            
                            if let documents = object!["documents"]! as? NSArray{
                                var documentId: Int?
                                var documentPath: String?
                                
                                for obj in documents {
                                    
                                    objectDoc = obj as? NSDictionary
                                    
                                    documentId = objectDoc!["documentId"]! as? Int
                                    documentPath = objectDoc!["documentPath"]! as? String
                                    
                                    self.documentsList.append(Documents(documentId: documentId!, documentPath: documentPath!))
                                    
                                }
                                
                            }
                            
                            
                            self.newsList.append(News(id: id!, title: title!, content: content!, image: image!, publishDate: publishDate!, documents: self.documentsList))
                            
                            self.documentsList = []
                            
                            
                        }
                    }
                    self.newsTable.reloadData()
                }
        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        if(!searchNews) {
            return newsList.count
        } else {
            return tempList.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let c = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsCell
        
        if(!searchNews) {
            if(newsList.count > 0) {
                c.newsTitle.text = newsList[indexPath.section].title
                c.newsImage.setShowActivityIndicator(true)
                c.newsImage.setIndicatorStyle(.gray)
                c.newsImage.sd_setImage(with: URL(string: "\(newsList[indexPath.section].image.replacingOccurrences(of: " ", with: "%20"))"))
                c.newsDate.text = newsList[indexPath.section].publishDate
                
                c.selectionStyle = .none
            }
        } else {
            if(tempList.count > 0) {
                c.newsTitle.text = tempList[indexPath.section].title
                c.newsImage.setShowActivityIndicator(true)
                c.newsImage.setIndicatorStyle(.gray)
                c.newsImage.sd_setImage(with: URL(string: "\(tempList[indexPath.section].image.replacingOccurrences(of: " ", with: "%20"))"))
                c.newsDate.text = tempList[indexPath.section].publishDate
                
                c.selectionStyle = .none
            }
        }
        return c
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(section == newsList.count - 1) {
            return 0.0
        }
        else {
            return 5.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.white
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        newsSelection = indexPath.section
        performSegue(withIdentifier: "newsDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let newScreen = segue.destination as? NewsDetails {
            if(!searchNews) {
                newScreen.news = newsList[newsSelection!]
            } else {
                newScreen.news = tempList[newsSelection!]
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 5.0
        }
        else {
            return 0.0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if(searchText.count > 0) {
            searchNews = true
            
            tempList.removeAll()
            
            for news in self.newsList {
                
                var query = searchText.lowercased()
                query = query.trimmingCharacters(in: .whitespacesAndNewlines)
                
                let queries = query.components(separatedBy: " ")
                
                var counter = 0
                for queryPart in queries {
                    if news.title.lowercased().range(of:queryPart) != nil {
                        counter = counter + 1
                    }
                }
                
                if(counter == queries.count) {
                    tempList.append(news)
                }
                
            }
            self.newsTable.reloadData()
        } else {
            searchNews = false
            self.newsTable.reloadData()

            searchBar.endEditing(true)
        }
    }
  
    
}


