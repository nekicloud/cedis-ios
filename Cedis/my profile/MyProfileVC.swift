//
//  MyProfileVC.swift
//  Cedis
//
//  Created by Administrator on 23/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import SDWebImage
import CropViewController

import Firebase

class MyProfileVC:BaseViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate {

    let dropDown = DropDown()
    var imagePicker = UIImagePickerController()
    
    private var croppedRect = CGRect.zero
    private var croppedAngle = 0
    
    private let imageView = UIImageView()
    
    private var image: UIImage?
    private var croppingStyle = CropViewCroppingStyle.default
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        profileImage.layer.cornerRadius = 90.0
        changeView.layer.cornerRadius = 30.0
        changePassBtn.layer.cornerRadius = 10.0
        logOutBtn.layer.cornerRadius = 10.0
        logOutBtn.layer.borderWidth = 0.5
        logOutBtn.layer.borderColor = Utils().hexStringToUIColor(hex: Constants.Colors.red).cgColor
        
        changePassBtn.titleLabel?.textAlignment = .center
//        profileImage.layer.borderWidth = 0.0
        profileImage.layer.borderColor = UIColor(displayP3Red: 212/255, green: 53/255, blue: 53/255, alpha: 0.6).cgColor
        
        imagePicker.delegate = self
        
//        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
//        self.navigationController?.navigationBar.isTranslucent = true

        firstLastName.text = Utils().formatName(fullName: userList[0].nameSurname)
        sector.text = userList[0].sector
        workPlace.text = userList[0].workPlace
        
        let changeProfilePic = UITapGestureRecognizer(target: self, action:  #selector (self.changeProfilePicAction))
        profileImage.isUserInteractionEnabled = true
        profileImage.addGestureRecognizer(changeProfilePic)
        
        
    }
    
    @objc func changeProfilePicAction(_ sender:UITapGestureRecognizer){
        
        dropDown.dataSource = ["Kamera", "Galerija"]
        dropDown.width = 300
        dropDown.show()
        
        setupCenteredDropDown()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        profileImage.setShowActivityIndicator(true)
        profileImage.setIndicatorStyle(.gray)
        profileImage.sd_setImage(with: URL(string: "\(userList[0].profileImage)"), placeholderImage: UIImage(named:"user"), options: .refreshCached)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var changeView: UIView!
    @IBOutlet weak var firstLastName: UILabel!
    @IBOutlet weak var workPlace: UILabel!
    @IBOutlet weak var sector: UILabel!
    @IBOutlet weak var changePassBtn: UIButton!
    @IBOutlet weak var logOutBtn: UIButton!
    
    @IBAction func changePassword(_ sender: Any) {
        
    }
    
    @IBAction func logOut(_ sender: Any) {
        let alert = UIAlertController(title: "Izloguj se", message: "Da li ste sigurni da želite da se izlogujete?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Da", style: UIAlertActionStyle.default, handler: { action in
            
            let domain = Bundle.main.bundleIdentifier!
            UserDefaults.standard.removePersistentDomain(forName: domain)
            UserDefaults.standard.synchronize()
            
            userList.removeAll()
            
            let instance = InstanceID.instanceID()
            instance.deleteID { (error) in
                print(error.debugDescription)
            }
            
            if let token = InstanceID.instanceID().token() {
                print("Token : \(token)");
                defaults.set(token, forKey: "token")
            } else {
                print("unable to fetch token");
            }
            
            Messaging.messaging().shouldEstablishDirectChannel = true

            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "login") as! LoginVC
            self.present(vc, animated: true, completion: nil)
            
//            self.navigationController?.setNavigationBarHidden(false, animated: false)
//            self.navigationController?.pushViewController(vc, animated: true)

            
        }))
        alert.addAction(UIAlertAction(title: "Otkaži", style: UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func changeProfilePicture(_ sender: Any) {
        
        dropDown.dataSource = ["Kamera", "Galerija"]
        dropDown.width = 300
        dropDown.show()
        
        setupCenteredDropDown()
    }
    
    
    func setupCenteredDropDown() {
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            if (index ==  0 ){
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    self.croppingStyle = .circular
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .camera;
                    self.imagePicker.allowsEditing = false
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            } else {
                if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                    self.croppingStyle = .circular
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .savedPhotosAlbum;
                    self.imagePicker.allowsEditing = false
                    self.present(self.imagePicker, animated: true, completion: nil)
                }
            }
        }
    }
    
    func setNewImage(image: UIImage){
        
        if (CheckConnection.isInternetAvailable() == true) {
            
            let headers = [
                "Authorization": "ApiKey \(userList[0].apiKey)",
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            
            let imgData = UIImageJPEGRepresentation(image, 0.2)!
            let date = Date()
            let calendar = Calendar.current
            let components = calendar.dateComponents([.hour, .minute, .second], from: date)
            
            let hour =  components.hour
            let minute = components.minute
            let seconds = components.second

            Alamofire.upload(multipartFormData: { multipartFormData in
                    multipartFormData.append(imgData, withName: "image",fileName: "file\(userList[0].id)\(hour! + minute! + seconds!).jpg", mimeType: "image/jpg")},
                                 to:"http://cedisklik.me/ws/_changeProfilePicture.php",
                                 method:.post,
                                 headers: headers)
                { (result) in
                    switch result {
                    case .success(let upload, _, _):
                        
                        upload.uploadProgress(closure: { (progress) in
                            print("Upload Progress: \(progress.fractionCompleted)")
                        })
                        
                        upload.responseJSON { response in
                            let requestData = response.result.value as! NSDictionary
                            
                            var code : String?
                            
                            code = String(describing: requestData["error_code"]!)
                            
                            if code == "1" {
                                
                                let error = String(describing: requestData["errors"]!)
                                
                            } else {
                                
                                let data = requestData["data"] as! NSDictionary
                                let imagePath = data["imagePath"] as! String
                                userList[0].profileImage = imagePath
                                
                                self.dismiss(animated: true, completion: nil)
                            }
                            
                        }
                        
                    case .failure(let encodingError):
                        let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                        alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil
                        ))
                        self.present(alert9, animated: true, completion: nil)
                        
                        print(encodingError)
                    }
                }
        }
        else {
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
        }
        
    }
    
    
    //-------------old one
    
//    @objc private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
//        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
//        profileImage.image = chosenImage
//
//        setNewImage()
//    }
    
    
    //------------cropping
    
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = (info[UIImagePickerControllerOriginalImage] as? UIImage) else { return }
        
        let cropController = CropViewController(croppingStyle: croppingStyle, image: image)
        cropController.delegate = self
        
        cropController.toolbarPosition = .top
        
        cropController.doneButtonTitle = "Sačuvaj"
        cropController.cancelButtonTitle = "Odustani"
        
        self.image = image
        self.croppingStyle = .circular
        picker.pushViewController(cropController, animated: true)

    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        
        setNewImage(image: image)
    }
    
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        layoutImageView()
    }
    
    public func layoutImageView() {
        guard imageView.image != nil else { return }
        
        let padding: CGFloat = 20.0
        
        var viewFrame = self.view.bounds
        viewFrame.size.width -= (padding * 2.0)
        viewFrame.size.height -= ((padding * 2.0))
        
        var imageFrame = CGRect.zero
        imageFrame.size = imageView.image!.size;
        
        if imageView.image!.size.width > viewFrame.size.width || imageView.image!.size.height > viewFrame.size.height {
            let scale = min(viewFrame.size.width / imageFrame.size.width, viewFrame.size.height / imageFrame.size.height)
            imageFrame.size.width *= scale
            imageFrame.size.height *= scale
            imageFrame.origin.x = (self.view.bounds.size.width - imageFrame.size.width) * 0.5
            imageFrame.origin.y = (self.view.bounds.size.height - imageFrame.size.height) * 0.5
            imageView.frame = imageFrame
        }
        else {
            self.imageView.frame = imageFrame;
            self.imageView.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        }
    }
}
