//
//  ActDocsCell.swift
//  Cedis
//
//  Created by Administrator on 24/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit

class ActDocsCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//
//        self.layer.shadowColor = UIColor.black.cgColor
//        self.layer.shadowOffset = CGSize(width: 1, height: 1)
//        self.layer.shadowOpacity = 0.3
//        self.layer.shadowRadius = 2.0
//        self.layer.masksToBounds = false
        
        
        holder.layer.cornerRadius = 8
        holder.layer.borderColor = Utils().hexStringToUIColor(hex: Constants.Colors.grey).cgColor
        holder.layer.borderWidth = 0.5
        
        docName.textColor = Utils().hexStringToUIColor(hex: Constants.Colors.red)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = UIEdgeInsetsInsetRect(contentView.frame, UIEdgeInsetsMake(0, 10, 0, 10))
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    @IBOutlet weak var docName: UILabel!
    @IBOutlet weak var holder: UIView!
    
}
