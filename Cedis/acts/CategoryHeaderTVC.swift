//
//  CategoryHeaderTVC.swift
//  Cedis
//
//  Created by Milos Lalatovic on 01/08/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit

class CategoryHeaderTVC: UITableViewCell  {

    @IBOutlet weak var holder: UIView!
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var categoryIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        holder.layer.cornerRadius = 8
        holder.layer.shadowColor = UIColor.black.cgColor
        holder.layer.shadowOpacity = 0.2
        holder.layer.shadowOffset = CGSize(width: -0.2, height: 0.2)
        holder.layer.shadowRadius = 8
        holder.layer.masksToBounds = false
        
//        categoryIcon.layer.cornerRadius = 20.0
        

    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
