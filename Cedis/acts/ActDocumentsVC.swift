//
//  ActDocumentsVC.swift
//  Cedis
//
//  Created by Administrator on 24/05/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage


class ActDocumentsVC:BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    var actCategories = [ActCategory]()
    var section = Int()
    var row = Int()
    
    let searchBar = UISearchBar()
    var searchActs : Bool = false
    var tempList = [ActCategory]()
    
    @IBOutlet weak var searchHolder: UIView!
    @IBOutlet weak var actDocsTable: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        actDocsTable.delegate = self
        actDocsTable.dataSource = self
        actDocsTable.tableFooterView = UIView()
        actDocsTable.separatorColor = .clear
        
        actDocsTable.rowHeight = UITableViewAutomaticDimension
        actDocsTable.estimatedRowHeight = 60
        
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        //        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.barTintColor = Utils().hexStringToUIColor(hex:Constants.Colors.red)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        searchBar.placeholder = "Pretraži"
        searchBar.frame = CGRect(x: 0, y: 0, width: (view.frame.size.width), height: 64)
        searchBar.barStyle = .default
        searchBar.isTranslucent = false
        searchBar.barTintColor = Utils().hexStringToUIColor(hex:Constants.Colors.red)
        searchBar.backgroundImage = UIImage()
        searchBar.enablesReturnKeyAutomatically = false
        searchBar.tintColor = .black
        
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.go
        searchHolder.addSubview(searchBar)

        topLayoutGuide.bottomAnchor.constraint(equalTo: searchBar.topAnchor).isActive = true
        
        self.title = "Interna regulativa"
        
        if (CheckConnection.isInternetAvailable() == true){
            getDocs()
        }
        else{
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
        }
        //actDocsTable.frame = UIEdgeInsetsInsetRect(self.view.frame, UIEdgeInsetsMake(0, 16, 0, 16))
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(!searchActs) {
            return actCategories.count
        } else {
            return tempList.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(!searchActs) {
            if(actCategories[section].opened) {
                return actCategories[section].documents.count + 1
            } else {
                return 1
            }
        } else {
            if(tempList[section].opened) {
                return tempList[section].documents.count + 1
            } else {
                return 1
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(!searchActs) {
            if(indexPath.row == 0) {
                let header = tableView.dequeueReusableCell(withIdentifier: "header") as! CategoryHeaderTVC
                header.categoryTitle.text = actCategories[indexPath.section].name
                
                if(actCategories[indexPath.section].opened) {
                    header.categoryIcon.image = UIImage(named: "ic_arrow_up")
                } else {
                    header.categoryIcon.image = UIImage(named: "ic_arrow_down")
                }
                return header
            } else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ActDocsCell", for: indexPath) as! ActDocsCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.docName.text = actCategories[indexPath.section].documents[indexPath.row - 1].title
                
                return cell
            }
        } else {
            if(indexPath.row == 0) {
                let header = tableView.dequeueReusableCell(withIdentifier: "header") as! CategoryHeaderTVC
                header.categoryTitle.text = tempList[indexPath.section].name
                
                if(tempList[indexPath.section].opened) {
                    header.categoryIcon.image = UIImage(named: "ic_arrow_up")
                } else {
                    header.categoryIcon.image = UIImage(named: "ic_arrow_down")
                }
                return header
            } else {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ActDocsCell", for: indexPath) as! ActDocsCell
                cell.selectionStyle = UITableViewCellSelectionStyle.none
                cell.docName.text = tempList[indexPath.section].documents[indexPath.row - 1].title
                
                return cell
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(!searchActs) {
            if(indexPath.row == 0) {
                if(actCategories[indexPath.section].opened) {
                    actCategories[indexPath.section].opened = false
                    let sections = IndexSet.init(integer: indexPath.section)
                    tableView.reloadSections(sections, with: .none)
                } else {
                    actCategories[indexPath.section].opened = true
                    let sections = IndexSet.init(integer: indexPath.section)
                    tableView.reloadSections(sections, with: .none)
                }
            } else {
                section = indexPath.section
                row = indexPath.row - 1
                
                let storyboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "webVC") as! ShowDocumentVC
                
                vc.documentTitle = actCategories[section].documents[row].title
                vc.documentUrl = actCategories[section].documents[row].filePath as NSString
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            if(indexPath.row == 0) {
                if(tempList[indexPath.section].opened) {
                    tempList[indexPath.section].opened = false
                    let sections = IndexSet.init(integer: indexPath.section)
                    tableView.reloadSections(sections, with: .none)
                } else {
                    tempList[indexPath.section].opened = true
                    let sections = IndexSet.init(integer: indexPath.section)
                    tableView.reloadSections(sections, with: .none)
                }
            } else {
                section = indexPath.section
                row = indexPath.row - 1
                
                let storyboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "webVC") as! ShowDocumentVC
                
                vc.documentTitle = tempList[section].documents[row].title
                vc.documentUrl = tempList[section].documents[row].filePath as NSString
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func getDocs(){
        
        let url : String = "http://cedisklik.me/ws/_getInternalActs.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSArray? {
                        
                        var object: NSDictionary?
                        for objSchedule in arraySchedule {
                            
                            object = objSchedule as? NSDictionary
                            
                            var docs = [ActDocument]()
                            
                            if let docsArray = object!["acts"] as! NSArray? {
                                
                                var act: NSDictionary?
                                for doc in docsArray {
                                    act = doc as? NSDictionary
                                    
                                    docs.append(ActDocument(id: (act!["actCategoryID"]! as? Int)!,
                                                            title: (act!["actTitle"]! as? String)!,
                                                            filePath: (act!["actFilePath"]! as? String)!))
                                }
                                
                                self.actCategories.append(ActCategory(opened: false,
                                                                 id: (object!["actCategoryID"]! as? Int)!,
                                                                 name: (object!["actCategoryName"]! as? String)!,
                                                                 icon: (object!["actCategoryIcon"]! as? String)!,
                                                                 documents: docs))
                                
                            }
                        }
                    }
                    
                    self.actDocsTable.reloadData()
                    
                    
                } else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { action in
                        //hud1.hide(animated: true, afterDelay: 0)
                    }))
                    self.present(alert9, animated: true, completion: nil)
                }
        }
    }
    
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if(searchText.count > 0) {
            searchActs = true
            tempList.removeAll()
            
            for actCategory in self.actCategories {
                
                var temp = actCategory
                
                var shouldAddCategory = false;
                var tempActs = [ActDocument]();
                
                for act in actCategory.documents {
                    var query = searchText.lowercased()
                    query = query.trimmingCharacters(in: .whitespacesAndNewlines)
                    
                    let queries = query.components(separatedBy: " ")
                    var counter = 0
                    
                    for queryPart in queries {
                        if (act.title.lowercased().range(of:queryPart)) != nil {
                            counter = counter + 1
                        }
                    }
                    
                    if(counter == queries.count) {
                        shouldAddCategory = true;
                        tempActs.append(act);
                    }
                }
                if (shouldAddCategory) {
                    temp.documents.removeAll()
                    temp.documents.append(contentsOf: tempActs);
                    tempList.append(temp);
                }
                
                self.actDocsTable.reloadData()
            }
            
            self.actDocsTable.reloadData()
        } else {
            searchActs = false
            self.actDocsTable.reloadData()
            
            searchBar.endEditing(true)
        }
    }
    
}
