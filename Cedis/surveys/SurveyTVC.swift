//
//  SurveyTVC.swift
//  Cedis
//
//  Created by Milos Lalatovic on 20/07/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit

class SurveyTVC: UITableViewCell {

    @IBOutlet weak var holder: UIView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
