//
//  QuestionVC.swift
//  Cedis
//
//  Created by Milos Lalatovic on 20/07/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import DLRadioButton

class QuestionVC:BaseViewController, UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var questionText: UILabel!
    
    var question = ""
    var position = 0
    var answers = [SurveyQuestionOption]()
    var i : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        scrollView.delegate = self
        questionText.text = question
        
        let width : Int = Int (scrollView.frame.size.width)
        var otherButtons : [DLRadioButton] = [];

        answers.forEach { data in
            
            var radioButton = DLRadioButton()
            radioButton.isMultipleSelectionEnabled = false

            radioButton = DLRadioButton(frame: CGRect(x: 0, y: i, width: width, height: 60))
            radioButton.setTitle(data.questionOptionValue ,for: .normal)
            
            radioButton.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping;
            
            radioButton.setTitleColor(Utils().hexStringToUIColor(hex: "5d5d5d"), for: .normal);
            radioButton.setTitleColor(Utils().hexStringToUIColor(hex: "C72028"), for: .selected);
            radioButton.iconColor = Utils().hexStringToUIColor(hex: "5d5d5d");
            radioButton.icon = UIImage.init(named: "ic_checkbox")!
            radioButton.iconSelected = UIImage.init(named: "ic_checkbox_selected")!
            radioButton.iconSize = 30.0
        
//            radioButton.indicatorColor = Utils().hexStringToUIColor(hex: "C72028");
            radioButton.contentHorizontalAlignment = UIControlContentHorizontalAlignment.left;
            radioButton.marginWidth = 20.0
            radioButton.tag = data.questionOptionID
            radioButton.addTarget(self, action: #selector(logSelectedButton), for: UIControlEvents.touchUpInside);
            
//            if(selectedOption != -1) {
//                if(selectedOption == data.questionOptionID) {
//                    radioButton.isSelected = true
//                    selectedOptions[position] = "\(radioButton.tag)";
//                }
//            }
            
            radioButton.otherButtons = otherButtons
            otherButtons.append(radioButton)

            scrollView.addSubview(radioButton)
            
            i = i + 60
            
            print(data)
        }
        
        scrollView.contentSize = CGSize(width: width, height: i)

        
    }
    
    @objc func logSelectedButton(radioButton : DLRadioButton) {
        if (radioButton.isMultipleSelectionEnabled) {
            print("OVO NE SMIJE DA SE DESI");
        } else {
            print("ODABRAN \(position) RADIO BUTTON");
            selectedOptions[position] = "\(radioButton.tag)";
        }
    }

}
