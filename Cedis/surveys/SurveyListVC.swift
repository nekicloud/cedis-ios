//
//  SurveyLIstViewController.swift
//  Cedis
//
//  Created by Milos Lalatovic on 20/07/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire

class SurveyListVC:BaseViewController, UITableViewDelegate, UITableViewDataSource  {
    
    @IBOutlet weak var noData: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var row : Int = 0
    var surveys = [Survey]()
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        getSurveys()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.separatorColor = .clear
        tableView.separatorStyle = .none
        tableView.reloadData()
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        
        self.title = "Ankete"
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return surveys.count
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        row = indexPath.row
        
        if(surveys[row].surveyStatus == 1) {
            performSegue(withIdentifier: "takeSurvey", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let newScreen = segue.destination as? TakeSurveyVC {
            newScreen.survey = surveys[row]
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "survey", for: indexPath) as! SurveyTVC
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none

        if(surveys[indexPath.row].surveyStatus == 1) {
            cell.title.textColor = Utils().hexStringToUIColor(hex: Constants.Colors.red)

            cell.holder.layer.cornerRadius = 5
            cell.holder.layer.shadowColor = UIColor.black.cgColor
            cell.holder.layer.shadowOpacity = 0.5
            cell.holder.layer.shadowOffset = CGSize(width: -1, height: 1)
            cell.holder.layer.shadowRadius = 5
            cell.holder.layer.masksToBounds = false
        } else {
            cell.title.textColor = Utils().hexStringToUIColor(hex: Constants.Colors.grey)
            cell.holder.layer.borderColor = Utils().hexStringToUIColor(hex: Constants.Colors.grey).cgColor
            cell.holder.layer.borderWidth = 1
            cell.holder.layer.cornerRadius = 5

        }
        
        cell.title.text = surveys[indexPath.row].surveyTitle
        cell.date.text = formatDate(date: surveys[indexPath.row].surveyCreationDate)
        cell.date.textColor = Utils().hexStringToUIColor(hex: Constants.Colors.grey)
        
        return cell
    }
    
    
    func getSurveys(){
        
        surveys.removeAll()
        let url : String = "http://cedisklik.me/ws/_getSurveys.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSArray? {
                        
                        var object: NSDictionary?
                        for objSchedule in arraySchedule {
                            
                            object = objSchedule as? NSDictionary
                            
                            var questions = [SurveyQuestion]()
                            
                            if let questionArray = object!["questions"] as! NSArray? {
                                
                                var act: NSDictionary?
                                for doc in questionArray {
                                    act = doc as? NSDictionary
                                    
                                    var options = [SurveyQuestionOption]()
                                    
                                    if let optionArray = act!["questionOptions"] as! NSArray? {
                                        
                                        var option: NSDictionary?
                                        for doc in optionArray {
                                            option = doc as? NSDictionary
                                            options.append(SurveyQuestionOption(questionOptionID: (option!["id"]! as? Int)!,
                                                                                questionOptionValue: (option!["value"]! as? String)!))
                                        }
                                    }
                                    
                                    questions.append(SurveyQuestion(questionID: (act!["questionID"]! as? Int)!,
                                                                    questionTitle: (act!["questionTitle"]! as? String)!,
                                                                    questionOptions: options,
                                                                    selectedOption : -1))
                                }
                            }
                            self.surveys.append(Survey(surveyID: (object!["surveyID"]! as? Int)!,
                                                       surveyTitle: (object!["surveyTitle"]! as? String)!,
                                                       surveyCreationDate: (object!["surveyCreationDate"]! as? String)!,
                                                       surveyStatus: (object!["surveyStatus"]! as? Int)!,
                                                       questions: questions))
                        }
                        
                        if(self.surveys.count > 0) {
                            self.tableView.reloadData()
                        } else {
                            self.tableView.isHidden = true
                        }
                    }
                } else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { action in
                        //hud1.hide(animated: true, afterDelay: 0)
                    }))
                    self.present(alert9, animated: true, completion: nil)
                }
            }
    }
}
