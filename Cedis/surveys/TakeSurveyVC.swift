//
//  TakeSurveyVC.swift
//  Cedis
//
//  Created by Milos Lalatovic on 20/07/2018.
//  Copyright © 2018 Amplitudo. All rights reserved.
//

import UIKit
import SwipeMenuViewController
import Alamofire

var questions = [String]()
var selectedOptions = [String]()

class TakeSurveyVC: SwipeMenuViewController {
    
    @IBOutlet weak var actions: UIView!
    @IBOutlet weak var close: UIButton!
    @IBOutlet weak var pages: UILabel!
    @IBOutlet weak var surveyName: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var curentPage: UILabel!
    @IBOutlet weak var divider: UILabel!
    
    @IBAction func close(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func next(_ sender: Any) {
        if(lastPage) {
            if(!selectedOptions.contains("-1")) {
                sendSurvey()
            } else {
                let alert1 = UIAlertController(title: "Obavještenje", message: "Nije odgovoreno na sva pitanja", preferredStyle: UIAlertControllerStyle.alert)
                alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  { action in
                    alert1.dismiss(animated: true, completion: nil)
                }))
                self.present(alert1, animated: true, completion: nil)
            }
        } else {
            swipeMenuView.jump(to: pageNumber + 1, animated: true)
        }
    }
    
    @IBAction func back(_ sender: Any) {
        swipeMenuView.jump(to: pageNumber - 1, animated: true)
    }
    
    var lastPage : Bool = false
    var pageNumber : Int = 0
    
    var options = SwipeMenuViewOptions()
    
    var survey = Survey(surveyID: 0, surveyTitle: "aktivna", surveyCreationDate: "datum", surveyStatus: 1, questions: [SurveyQuestion]())
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        
        selectedOptions = [String]()
        questions = [String]()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        survey.questions.forEach { data in
            
            questions.append("\(data.questionID)")
            selectedOptions.append("-1")
        }
        
        surveyName.text = survey.surveyTitle
        surveyName.textColor = Utils().hexStringToUIColor(hex: Constants.Colors.red)
        
        curentPage.text = "\(1)"
        curentPage.textColor = Utils().hexStringToUIColor(hex: Constants.Colors.red)
        
        pages.text = "\(survey.questions.count)"
        pages.textColor = Utils().hexStringToUIColor(hex: Constants.Colors.grey)
        divider.textColor = Utils().hexStringToUIColor(hex: Constants.Colors.grey)
        
        var position = 0
        self.survey.questions.forEach { data in
            
            let storyboard: UIStoryboard = UIStoryboard(name: "Surveys", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "question") as! QuestionVC
            
            vc.answers = data.questionOptions
            vc.question = data.questionTitle
            vc.position = position
            
            position = position + 1
            self.addChildViewController(vc)
        }
        
        //        options.tabView.needsAdjustWidth = true
        self.options.tabView.height = 60
        self.options.tabView.style = .segmented
        self.options.tabView.itemView.selectedTextColor = Utils().hexStringToUIColor(hex: Constants.Colors.dark)
        self.options.tabView.itemView.textColor = Utils().hexStringToUIColor(hex: Constants.Colors.grey)
        //        options.tabView.itemView.  = Utils().hexStringToUIColor(hex: Constants.Colors.gray)
        self.options.tabView.addition = .none
        
        self.reload()
        
        self.view.bringSubview(toFront: self.surveyName)
        self.view.bringSubview(toFront: self.actions)
        self.view.bringSubview(toFront: self.close)
    }
    
    private func reload() {
        swipeMenuView.reloadData(options: options)
    }
    
    // MARK: - SwipeMenuViewDelegate
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewWillSetupAt currentIndex: Int) {
        super.swipeMenuView(swipeMenuView, viewWillSetupAt: currentIndex)
        print("will setup SwipeMenuView")
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewDidSetupAt currentIndex: Int) {
        super.swipeMenuView(swipeMenuView, viewDidSetupAt: currentIndex)
        print("did setup SwipeMenuView")
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, willChangeIndexFrom fromIndex: Int, to toIndex: Int) {
        super.swipeMenuView(swipeMenuView, willChangeIndexFrom: fromIndex, to: toIndex)
        print("will change from section\(fromIndex + 1)  to section\(toIndex + 1)")
        
        if(toIndex == 0) {
            backButton.isHidden = true
        } else {
            backButton.isHidden = false
        }
        
        if(toIndex == survey.questions.count - 1) {
            lastPage = true
            nextButton.setImage(UIImage.init(named: "ic_finish"), for: .normal)
        } else {
            lastPage = false
            nextButton.setImage(UIImage.init(named: "ic_next"), for: .normal)
        }
        
        curentPage.text = "\(toIndex + 1)"
        
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, didChangeIndexFrom fromIndex: Int, to toIndex: Int) {
        super.swipeMenuView(swipeMenuView, didChangeIndexFrom: fromIndex, to: toIndex)
        print("did change from section\(fromIndex + 1)  to section\(toIndex + 1)")
        pageNumber = toIndex
    }
    
    
    // MARK - SwipeMenuViewDataSource
    
    override func numberOfPages(in swipeMenuView: SwipeMenuView) -> Int {
        if(survey.surveyID == 0) {
            return 1
        } else {
            return survey.questions.count
        }
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, titleForPageAt index: Int) -> String {
        if(childViewControllers.isEmpty) {
            return ""
        } else {
            return childViewControllers[index].title ?? ""
        }
    }
    
    override func swipeMenuView(_ swipeMenuView: SwipeMenuView, viewControllerForPageAt index: Int) -> UIViewController {
        if(childViewControllers.isEmpty) {
            let storyboard: UIStoryboard = UIStoryboard(name: "Surveys", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "question") as! QuestionVC
            return vc
        } else {
            let vc = childViewControllers[index]
            vc.didMove(toParentViewController: self)
            return vc
        }
    }
    
    func sendSurvey() {
        
        let parameters: Parameters = [
            "questions": "[\(questions.joined(separator: ","))]", // "1-2-3",
            "options": "[\(selectedOptions.joined(separator: ","))]",
            "surveyId": "\(survey.surveyID)"
        ]
        
        let url : String = "http://cedisklik.me/ws/_surveyAnswers.php?"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        var error : String = ""
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    var code : String?
                    code = String(describing: requestData["error_code"]!)
                    
                    if code == "1" {
                        
                        let alert1 = UIAlertController(title: "Obavještenje", message: "\(requestData["errors"]!)", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  { action in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert1, animated: true, completion: nil)
                        
                    }
                    else {
                        
                        let alert1 = UIAlertController(title: "Obavještenje", message: "Anketa uspješno popunjena", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  { action in
                            self.navigationController?.popViewController(animated: true)
                            self.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert1, animated: true, completion: nil)
                    }
                }
                else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert9, animated: true, completion: nil)
                }
                
        }
    }
    
    
    func surveyDetails() {
        
        let url : String = "http://cedisklik.me/ws/_getSurveyAnswersForUser.php?surveyID=\(survey.surveyID)";
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        var error : String = ""
        
        Alamofire.request(url, method: .get, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    var code : String?
                    code = String(describing: requestData["error_code"]!)
                    
                    if code == "1" {
                        
                        let alert1 = UIAlertController(title: "Obavještenje", message: "\(requestData["errors"]!)", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  { action in
                            self.navigationController?.popViewController(animated: true)
                        }))
                        self.present(alert1, animated: true, completion: nil)
                        
                    }
                    else {
                        
                        var alreadySelectedOptions = [SurveySelectedOption]()

                        if let arraySchedule = requestData["data"] as! NSArray? {
                            
                            var object: NSDictionary?
                            for objSchedule in arraySchedule {
                                                                
                                object = objSchedule as? NSDictionary
                                alreadySelectedOptions.append(SurveySelectedOption(questionId: (object!["questionId"]! as? Int)!,
                                                                                   optionId: (object!["optionId"]! as? Int)!))
                            }
                            
                            var position = 0
                            self.survey.questions.forEach { data in
                                
                                let storyboard: UIStoryboard = UIStoryboard(name: "Surveys", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "question") as! QuestionVC
                                
                                vc.answers = data.questionOptions
                                vc.question = data.questionTitle
                                vc.position = position
//                                if(arraySchedule.count > 0) {
//                                    vc.selectedOption = alreadySelectedOptions[position].optionId
//                                }
                                position = position + 1
                                self.addChildViewController(vc)
                            }
                            
                            //        options.tabView.needsAdjustWidth = true
                            self.options.tabView.height = 60
                            self.options.tabView.style = .segmented
                            self.options.tabView.itemView.selectedTextColor = Utils().hexStringToUIColor(hex: Constants.Colors.dark)
                            self.options.tabView.itemView.textColor = Utils().hexStringToUIColor(hex: Constants.Colors.grey)
                            //        options.tabView.itemView.  = Utils().hexStringToUIColor(hex: Constants.Colors.gray)
                            self.options.tabView.addition = .none
                            
                            self.reload()
                            
                            self.view.bringSubview(toFront: self.surveyName)
                            self.view.bringSubview(toFront: self.actions)
                            self.view.bringSubview(toFront: self.close)
                            
                        }
                    }
                }
                else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert9, animated: true, completion: nil)
                }
                
        }
    }
}
