//
//  CommentsLikesTBC.swift
//  ZoranKesic
//
//  Created by Administrator on 17/08/2017.
//  Copyright © 2017 Amplitudo. All rights reserved.
//

import UIKit

class CommentsLikesTBC: UITabBarController, UITabBarControllerDelegate {
    
    
    let myClassInstance = CommentsVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "HelveticaNeue-Light", size: 19)!]
        view.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.dark)

        
        let appearance = UITabBarItem.appearance()
        let appearanceActive = UITabBarItem.appearance()

        let activeTextAttributes: [NSAttributedStringKey: Any] = [
            .foregroundColor : UIColor.white,
            .font : UIFont(name:"Montserrat-Regular", size: 14.0)
        ]

        let textAttributes: [NSAttributedStringKey: Any] = [
            .foregroundColor : Utils().hexStringToUIColor(hex: Constants.Colors.grey),
            .font : UIFont(name:"Montserrat-Regular", size: 14.0)
        ]
        
        appearance.setTitleTextAttributes(textAttributes, for: .normal)
        appearanceActive.setTitleTextAttributes(activeTextAttributes, for: .selected)
        
    }
    
//    override func viewDidDisappear(_ animated: Bool) {
//        let selectedColor   = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 1.0)
//        let normalColor = UIColor(red: 255.0/255.0, green: 255.0/255.0, blue: 255.0/255.0, alpha: 0.4)
//
//        self.tabBar.tintColor = selectedColor
//        self.tabBar.unselectedItemTintColor = normalColor
//
//        let selectedAttributes = [NSAttributedStringKey.font:
//            UIFont(name: "Montserrat-Regular", size: 12.0)!,
//                                  NSAttributedStringKey.foregroundColor: selectedColor] as! [NSAttributedStringKey: Any]
//
//        let normalAttributes = [NSAttributedStringKey.font:
//            UIFont(name: "Montserrat-Regular", size: 12.0)!,
//                                NSAttributedStringKey.foregroundColor: normalColor] as! [NSAttributedStringKey: Any]
//
//        UITabBarItem.appearance().setTitleTextAttributes(normalAttributes, for: .normal)
//        UITabBarItem.appearance().setTitleTextAttributes(selectedAttributes, for: .selected)
//    }
    
    // UITabBarDelegate
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        
//        if(item == tabBar.selectedItem) {
//            if(self.pulleyViewController?.drawerPosition != .open ) {
//                self.pulleyViewController?.setDrawerPosition(position: .open, animated: true)
//            } else {
//                self.pulleyViewController?.setDrawerPosition(position: .collapsed, animated: true)
//            }
//        } else {
//            if(self.pulleyViewController?.drawerPosition != .open ) {
//                self.pulleyViewController?.setDrawerPosition(position: .collapsed, animated: true)
//            } else {
//                self.pulleyViewController?.setDrawerPosition(position: .open, animated: true)
//            }
//        }
        
//        sto se pulley-a tice, collapsed i partial visine iste kao i visina taba = 50
//        to su jedina podesavanja zasad
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        var tabFrame:CGRect = self.tabBar.frame
        tabFrame.origin.y = self.view.frame.origin.y
        tabFrame.size.height = 50
        self.tabBar.frame = tabFrame
    }
    
    
}
