//
//  ShowPictureVC.swift
//  ZoranKesic
//
//  Created by Administrator on 17/08/2017.
//  Copyright © 2017 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire
import Pulley

class ShowPictureVC: BaseViewController, UIScrollViewDelegate, PulleyPrimaryContentControllerDelegate {
   
    @IBOutlet weak var likeIcon: UIImageView!
    @IBOutlet weak var likeBtn: UIView!
    @IBOutlet weak var back: UIView!
    
    @objc func addTapped(_ sender:UITapGestureRecognizer){
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Surveys", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "surveyList") as! SurveyListVC
        //        self.present(vc, animated: true, completion: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBOutlet weak var scrollView: UIScrollView!

    
    @objc func backAction(_ sender:UITapGestureRecognizer){
        self.dismiss(animated: true, completion: nil)
    }
    
    func drawerPositionDidChange(drawer: PulleyViewController, bottomSafeArea: CGFloat) {
        
        if drawer.drawerPosition == .collapsed   {
            print("collapsed")
        } else if drawer.drawerPosition == .open   {
            print("open")
        } else if drawer.drawerPosition == .partiallyRevealed   {
            print("partiallyRevealed")
        } else if drawer.drawerPosition == .closed   {
            self.pulleyViewController?.setDrawerPosition(position: .collapsed, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pulleyViewController?.delegate = self
        
        
        //------back click
        let gestureBack = UITapGestureRecognizer(target: self, action:  #selector (self.backAction))
        back.isUserInteractionEnabled = true
        back.addGestureRecognizer(gestureBack)
        
        //------like pic click
        let gestureLike = UITapGestureRecognizer(target: self, action:  #selector (self.likeDislike))
        likeBtn.isUserInteractionEnabled = true
        likeBtn.addGestureRecognizer(gestureLike)
        
        
        self.scrollView.delegate = self
        self.automaticallyAdjustsScrollViewInsets = false
        
        view.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.dark)
        
        var xCor = 0.0 as CGFloat
        self.scrollView.frame = CGRect(x:self.scrollView.frame.origin.x, y:self.scrollView.frame.origin.y, width:self.view.frame.width, height:self.view.frame.height - 111 )
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        let scrollViewHeight:CGFloat = self.scrollView.frame.height
        
        scrollView.isPagingEnabled = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadLikes"), object: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadComments"), object: nil)
            
            if(photosList[photoSelection!].isLikedByThisUser) {
                self.likeIcon.image = UIImage(named: "Asset43")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "likedByThisUser"), object: nil)
            } else {
                self.likeIcon.image = UIImage(named: "Asset10")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notLikedByThisUser"), object: nil)
            }
        }
        
        for img in photosList {

            let imgOne = UIImageView(frame: CGRect(x:xCor, y:0,width:scrollViewWidth, height:scrollViewHeight))
            imgOne.sd_setImage(with: URL(string: "\(img.filePath)"))
            imgOne.contentMode = .scaleAspectFit
            imgOne.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.dark)

            self.scrollView.addSubview(imgOne)
            xCor = xCor + scrollViewWidth
//
//            //let imgOneView = UIView(frame: CGRect(x: imgOne.frame.size.width - 38.0, y: 15.0, width: 23.0, height: 20.0 ))
//            let buttonLike = UIButton(frame: CGRect(x: imgOne.frame.size.width - 38.0, y: 15.0, width: 23.0, height: 20.0 ))
//            if img.isLikedByThisUser == true {
//                buttonLike.setBackgroundImage(UIImage(named: "Asset43"), for: .normal)
//                buttonLike.tag = 1
//            }
//            else {
//                buttonLike.setBackgroundImage(UIImage(named: "Asset10"), for: .normal)
//                buttonLike.tag = 0
//            }
//
//            //imgOneView.backgroundColor = UIColor.green
//
//            //buttonLike.backgroundColor = UIColor.red
//            buttonLike.titleLabel?.text = "\(img.id)***\(img.id)"
//            buttonLike.addTarget(self, action:#selector(ShowPictureVC.likeDislike(_:)), for: UIControlEvents.touchUpInside)
//            //imgOneView.addSubview(buttonLike)
//            //imgOneView.isUserInteractionEnabled = true
            imgOne.isUserInteractionEnabled = true
//            imgOne.addSubview(buttonLike)
//            //buttonLike.superview?.superview?.bringSubview(toFront: buttonLike)
        }
        
        let numberOfSlides = CGFloat(photosList.count)
        self.scrollView.contentSize = CGSize(width:self.scrollView.frame.width * numberOfSlides, height:self.scrollView.frame.size.height - 50)
        
        let x = CGFloat(photoSelection!) * scrollView.frame.size.width
        scrollView.setContentOffset(CGPoint(x: x, y:0), animated: true)
        
        
    }
    
    @objc func likeDislike(_ sender: UIButton){
        
        if (CheckConnection.isInternetAvailable() == true) {
            
            let imageID = photosList[photoSelection!].id
            let url : String = "http://cedisklik.me/ws/_likePhoto.php?photoID=\(imageID)"
            
            let headers = [
                "Authorization": "ApiKey \(userList[0].apiKey)",
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
                .validate()
                .responseJSON { (response) -> Void in
                    
                    if let requestData = response.result.value as! NSDictionary?{
                        
                        var code : String?
                        code = String(describing: requestData["error_code"]!)
                        
                        if (code == "0" ){
                            if(!photosList[photoSelection!].isLikedByThisUser) {
                                photosList[photoSelection!].isLikedByThisUser = true
                                self.likeIcon.image = UIImage(named: "Asset43")
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "likedByThisUser"), object: nil)
                            } else {
                                photosList[photoSelection!].isLikedByThisUser = false
                                self.likeIcon.image = UIImage(named: "Asset10")
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notLikedByThisUser"), object: nil)
                            }
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadLikes"), object: nil)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadComments"), object: nil)
                        }
                        else{
                            let alert1 = UIAlertController(title: "Greška", message: "Pokušajte ponovo!", preferredStyle: UIAlertControllerStyle.alert)
                            alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  nil))
                            self.present(alert1, animated: true, completion: nil)
                        }
                    }
                    else {
                        let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                        alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
                        self.present(alert9, animated: true, completion: nil)
                    }
            }
        } else {
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
        }
        
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let photoPosition = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        
        photoSelection = photoPosition
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadLikes"), object: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "loadComments"), object: nil)
        
        if(photosList[photoSelection!].isLikedByThisUser) {
            self.likeIcon.image = UIImage(named: "Asset43")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "likedByThisUser"), object: nil)
        } else {
            self.likeIcon.image = UIImage(named: "Asset10")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "notLikedByThisUser"), object: nil)
        }
        
        
    }
    
}
