//
//  LikeCell.swift
//  ZoranKesic
//
//  Created by Administrator on 21/08/2017.
//  Copyright © 2017 Amplitudo. All rights reserved.
//

import UIKit

class LikeCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        profilImage.layer.cornerRadius = 20.0
    }

    
    
    @IBOutlet weak var profilImage: UIImageView!

    @IBOutlet weak var userName: UILabel!
}
