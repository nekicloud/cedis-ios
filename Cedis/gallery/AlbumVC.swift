//
//  OpenAlbum.swift
//  ZoranKesic
//
//  Created by Administrator on 16/08/2017.
//  Copyright © 2017 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import Pulley

struct Photo {
    
    var id: Int
    var filePath: String
    var isLikedByThisUser: Bool
    var comments: Int
    var likes: Int
}

var photosList = [Photo]()
var photoSelection: Int?

class AlbumVC:BaseViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    var selectedAlbum: Int = 0
    var selectImage = 0
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.backgroundColor = .clear
        
        view.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.dark)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        photosList = []
        
        collectionView.reloadData()
        
        if (CheckConnection.isInternetAvailable() == true) {
            getPhotos()
        } else {
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)

        }
        self.title = albumList[albumSelection!].albumName.uppercased()
//        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photosList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "openAlbumCell", for: indexPath as IndexPath) as! OpenAlbumCell
        
        cell.photo.setShowActivityIndicator(true)
        cell.photo.setIndicatorStyle(.gray)
        
        cell.photo.sd_setImage(with: URL(string: "\(photosList[indexPath.item].filePath)"))
        
        return cell
    }
    
    func getPhotos(){
        
        let url : String = "http://cedisklik.me/ws/_getPhotosFromAlbum.php?albumID=\(selectedAlbum)"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSArray? {
                        
                        
                        var object: NSDictionary?
                        
                        for objSchedule in arraySchedule {
                            
                            object = objSchedule as? NSDictionary
                            
                            photosList.append(Photo(id: (object!["id"]! as? Int)!,
                                                    filePath: (object!["filePath"]! as? String)!,
                                                    isLikedByThisUser: (object!["isLikedByThisUser"]! as? Bool)!,
                                                     comments: (object!["comments"]! as? Int)!,
                                                     likes: (object!["likes"]! as? Int)!))
                            
                        }
                    }
                    
                    self.collectionView.reloadData()
                    
                    
                }
                else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { action in
                        //hud1.hide(animated: true, afterDelay: 0)
                    }))
                    self.present(alert9, animated: true, completion: nil)
                }
                
//                if photosList.count > 0 {
//                    self.refreshView.isHidden = true
//                }
//                else {
//                    self.refreshView.isHidden = false
//                }
                
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (CheckConnection.isInternetAvailable() == true) {
            
            photoSelection = indexPath.item
            
            let storyboard: UIStoryboard = UIStoryboard(name: "Gallery", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "showPic") as! Pulley.PulleyViewController
            self.present(vc, animated: true, completion: nil)
//            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
            
            
        }
        
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let destination = segue.destination as? ShowPictureVC {
//            destination.photoNo = photoSelection!
//        }
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width  = self.view.frame.size.width
        // in case you you want the cell to be 40% of your controllers view
        return CGSize(width: width * 0.31 as CGFloat, height: width * 0.31 as CGFloat)
    }
    
    
    
    @IBOutlet weak var collectionView: UICollectionView!
}
