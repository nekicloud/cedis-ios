//
//  AlbumsVC.swift
//  ZoranKesic
//
//  Created by Administrator on 16/08/2017.
//  Copyright © 2017 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
//import MBProgressHUD

struct Album{
    
    var albumID: Int
    var albumName: String
    var albumThumb: String
    
}

var albumSelection: Int?
var albumList = [Album]()

class GalleryVC:BaseViewController, UITableViewDelegate, UITableViewDataSource {

    override func viewWillAppear(_ animated: Bool) {

//        self.tabBarController?.tabBar.isHidden = true
        navigationController?.setNavigationBarHidden(false, animated: false)
        
        
    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        self.tabBarController?.tabBar.isHidden = false
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Galerija"

        albumTable.delegate = self
        albumTable.dataSource = self
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        view.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.dark)
        
        albumTable.separatorColor = .clear
        albumTable.separatorStyle = .none
        albumTable.backgroundColor = .clear
        
        albumList = [Album]()

        
        if (CheckConnection.isInternetAvailable()) {
            albumList = []
            getAlbums()
        } else {
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
        }
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    
    func getAlbums(){
        
        let url : String = "http://cedisklik.me/ws/_getAlbums.php"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSArray? {
                        
                        var object: NSDictionary?
                        
                        for objSchedule in arraySchedule {
                            object = objSchedule as? NSDictionary

                            albumList.append(Album(albumID: (object!["albumID"]! as? Int)!,
                                                   albumName: (object!["albumName"]! as? String)!,
                                                   albumThumb: (object!["albumThumb"]! as? String)!))
                        }
                        
                    }
                    self.albumTable.reloadData()
                    
                }
                else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { action in
                        //hud1.hide(animated: true, afterDelay: 0)
                    }))
                    self.present(alert9, animated: true, completion: nil)
                }
        }
    }


    @IBOutlet weak var albumTable: UITableView!
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return albumList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "albumCell", for: indexPath) as! AlbumCellTVC
        c.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.dark)

        c.albumName.text = albumList[indexPath.section].albumName.uppercased()
        
        c.albumImage.setShowActivityIndicator(true)
        c.albumImage.setIndicatorStyle(.white)
        c.albumImage.sd_setImage(with: URL(string: "\(albumList[indexPath.section].albumThumb)"))
        return c
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        albumSelection = indexPath.section
        let gallery = albumList[albumSelection!].albumID
        performSegue(withIdentifier: "openAlbum", sender: gallery)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? AlbumVC {
            if let galleryId = sender as? Int {
                destination.selectedAlbum = galleryId
            }
        }
    }


}
