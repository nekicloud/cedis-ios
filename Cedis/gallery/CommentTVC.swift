//
//  CommentsCell.swift
//  ZoranKesic
//
//  Created by Administrator on 21/08/2017.
//  Copyright © 2017 Amplitudo. All rights reserved.
//

import UIKit

class CommentTVC: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        userImage.layer.cornerRadius = 20.0
        
    }

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var dateOfComment: UILabel!
    @IBOutlet weak var comment: UILabel!
    
    
    @IBOutlet weak var deleteComment: UIView!
    
}
