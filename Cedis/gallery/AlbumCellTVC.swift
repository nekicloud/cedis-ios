//
//  AlbumCellTVC.swift
//
//  Created by Administrator on 16/08/2017.
//  Copyright © 2017 Amplitudo. All rights reserved.
//

import UIKit

class AlbumCellTVC: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBOutlet weak var albumImage: UIImageView!
    @IBOutlet weak var albumName: UILabel!
    

}
