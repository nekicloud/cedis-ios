//
//  LikesVC.swift
//  ZoranKesic
//
//  Created by Administrator on 21/08/2017.
//  Copyright © 2017 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
//import MBProgressHUD


class LikesVC:BaseViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    var likeList = [Like]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.dark)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.separatorColor = .clear
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        
        tableView.estimatedRowHeight = 50.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(loadList(_:)),
                                               name:NSNotification.Name(rawValue: "loadLikes"),
                                               object: nil)
        
        getLikes()
        
    }
    
    
    
    @objc func loadList(_ sender: UIButton){
        likeList.removeAll()
        getLikes()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
//        getLikes()
//        AppUtility.lockOrientation(.portrait)
        
    }

    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return likeList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "likeCell", for: indexPath) as! LikeCell
        
        if(likeList.count > 0) {
            c.profilImage.setShowActivityIndicator(true)
            c.profilImage.setIndicatorStyle(.gray)
            
            c.profilImage.sd_setImage(with: URL(string: "\(likeList[indexPath.section].imagePath)"), placeholderImage: UIImage(named:"ic_person_outline_48pt"))
            //c.profilImage.sd_setImage(with: URL(string: "\(likeList[indexPath.section].imagePath)"))
            
            c.userName.text = likeList[indexPath.section].username//.uppercased()
        }
        
        return c
    }
    
    func getLikes(){
        
        if (CheckConnection.isInternetAvailable() == true) {
            
            likeList = []
            
            let url : String = "http://cedisklik.me/ws/_getLikes.php?photoID=\(photosList[photoSelection!].id)"
            
            let headers = [
                "Authorization": "ApiKey \(userList[0].apiKey)",
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
                .validate()
                .responseJSON { (response) -> Void in
                    
                    if let requestData = response.result.value as! NSDictionary? {
                        
                        if let arraySchedule = requestData["data"] as! NSArray? {
                            
                            var object: NSDictionary?
                            
                            for objSchedule in arraySchedule {
                                
                                object = objSchedule as? NSDictionary
                                
                                self.likeList.append(Like(username: object?["nameSurname"]! as! String, imagePath: object?["profileImage"]! as! String!))
                                
                            }
                            
                        }
                        self.tableView.reloadData()
                        self.tableViewScrollToBottom(animated: true)
                        self.tabBarController?.tabBar.items?[1].title = "LAJKOVI(\(self.likeList.count))"
                    }
                    else {
                        let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                        alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { action in
                            //hud1.hide(animated: true, afterDelay: 0)
                        }))
                        self.present(alert9, animated: true, completion: nil)
                    }
//                    print(likeList)
                    
                    
                    
                    
            }
        } else {
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
        }
    }
    
    
    func tableViewScrollToBottom(animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            let numberOfSections = self.tableView.numberOfSections
            
            if numberOfSections > 0 {
            let numberOfRows = self.tableView.numberOfRows(inSection: numberOfSections-1)
            
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                }
                
            }
        }
    }
    
}
