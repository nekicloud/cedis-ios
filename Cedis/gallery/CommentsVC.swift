//
//  CommentsVC.swift
//  ZoranKesic
//
//  Created by Administrator on 17/08/2017.
//  Copyright © 2017 Amplitudo. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import Pulley

class CommentsVC:BaseViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    var commentId = 0
    var commentList = [Comment]()

    var activeField: UITextView?
    
    @IBOutlet weak var stackBottom: NSLayoutConstraint!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var commentView: UITextView!
    @IBOutlet weak var outCommentView: UIView!
    
    @IBAction func addComment(_ sender: Any) {
        if(commentView.text.count > 0) {
            insertComment()
            commentView.endEditing(true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = Utils().hexStringToUIColor(hex: Constants.Colors.dark)
        
        tableView.separatorColor = .clear
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        
        tableView.delegate = self
        tableView.dataSource = self
        
        commentView.delegate = self
        commentView.text = "Unesi komentar"
        commentView.textColor = UIColor.lightGray
        
        commentView.isScrollEnabled = true
        
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(CommentsVC.loadList(notification:)),
                                               name:NSNotification.Name(rawValue: "loadComments"),
                                               object: nil)
        
        self.tabBarController?.selectedIndex = 1
        self.tabBarController?.selectedIndex = 0
    }
    
    
    @objc func loadList(notification: NSNotification){
        
        self.tabBarController?.tabBar.items?[0].title = "KOMENTARI(\(photosList[photoSelection!].comments))"
        self.tabBarController?.tabBar.items?[1].title = "LAJKOVI(\(photosList[photoSelection!].likes))"
        
        self.tabBarController?.tabBar.barTintColor = Utils().hexStringToUIColor(hex: Constants.Colors.red)
        
        
        commentList.removeAll()
        getComments()
    }
    
    deinit {
        self.deregisterFromKeyboardNotifications()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.deregisterFromKeyboardNotifications()
        //AppUtility.lockOrientation(.all)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        commentList.removeAll()
        
        //        getComments()
        self.registerForKeyboardNotifications()
        
        self.outCommentView.layer.borderWidth = 0.5
        self.outCommentView.layer.cornerRadius = 10
        self.outCommentView.layer.borderColor = UIColor.lightGray.cgColor
        self.outCommentView.backgroundColor = .white
        
        self.tabBarController?.tabBar.items?[0].title = "KOMENTARI(\(photosList[photoSelection!].comments))"
        self.tabBarController?.tabBar.items?[1].title = "LAJKOVI(\(photosList[photoSelection!].likes))"
        
        commentList.removeAll()
        getComments()
        
        
    }
    
    @IBDesignable class UITextViewFixed: UITextView {
        override func layoutSubviews() {
            super.layoutSubviews()
            setup()
        }
        func setup() {
            textContainerInset = UIEdgeInsets.zero
            textContainer.lineFragmentPadding = 0
        }
    }
    
    
    
    func getComments() {
        
        let url : String = "http://cedisklik.me/ws/_getComments.php?photoID=\(photosList[photoSelection!].id)"
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        
        Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary? {
                    
                    if let arraySchedule = requestData["data"] as! NSArray? {
                        
                        if arraySchedule.count != 0 {
                            
                            var object: NSDictionary?
                            
                            for objSchedule in arraySchedule {
                                object = objSchedule as? NSDictionary
                                
                                self.commentList.append(Comment(userID: (object?["userId"]! as? Int)!,
                                                           commentID: (object?["commentID"]! as? Int)!,
                                                           username: object?["username"]! as! String,
                                                           imagePath: object?["imagePath"]! as! String,
                                                           commentText: object?["commentText"]! as! String,
                                                           creationDate: object?["creationDate"]! as! String))
                            }
                            
                            
                            //self.tableViewScrollToBottom(animated: true)
                        } else {
                            //TODO OVO TREBA ISPRAVITI
                            //                            let alert9 = UIAlertController(title: "Obavještenje", message: "Nema komentara", preferredStyle: UIAlertControllerStyle.alert)
                            //                            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { action in
                            //                                //hud1.hide(animated: true, afterDelay: 0)
                            //                            }))
                            //                            self.present(alert9, animated: true, completion: nil)
                        }
                        
                        self.tableView.reloadData()
                        self.tableViewScrollToBottom(animated: true)
                        self.tabBarController?.tabBar.items?[0].title = "KOMENTARI(\(self.commentList.count))"
                    }
                }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return commentList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let c = tableView.dequeueReusableCell(withIdentifier: "commentsCell", for: indexPath) as! CommentTVC
        
        c.selectionStyle = .none
        
        c.backgroundColor = UIColor.clear
        if(commentList.count > 0){
            
            c.userImage.setShowActivityIndicator(true)
            c.userImage.setIndicatorStyle(.gray)
            c.userImage.sd_setImage(with: URL(string: "\(commentList[indexPath.section].imagePath)"), placeholderImage: UIImage(named:"ic_person_outline_48pt"))
            
            c.userName.text = commentList[indexPath.section].username
            
            /*formatiranje datuma - start*/
            var comDate = commentList[indexPath.section].creationDate
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dueDate2 = dateFormatter2.date(from: comDate)
            
            let calender = Calendar.current
            let components = calender.dateComponents([.year,.month,.day,.hour,.minute,.second], from: dueDate2!)
            let commentDate = "\(String(format: "%02d", components.day!)).\(String(format: "%02d", components.month!)).\(String(format: "%02d",     components.year!))"
            /*formatiranje datuma - end*/
            
            c.dateOfComment.text = "\(commentDate)"//" u \(commentTime)"
            
            if (userList[0].id == Int (commentList[indexPath.section].userID)) {
                c.deleteComment.isHidden = false
            }  else {
                c.deleteComment.isHidden = true
            }
            
            c.comment.text = commentList[indexPath.section].commentText.decodeEmoji
            
            c.deleteComment.tag = commentList[indexPath.section].commentID
            
            
            let gesture = UITapGestureRecognizer(target: self, action:  #selector (CommentsVC.deleteAction(_:)))
            c.deleteComment.addGestureRecognizer(gesture)
            
//            c.deleteComment.addTarget(self, action: #selector(CommentsVC.deleteComment(_:)), for: .touchUpInside)
            
            //c.frame.size.height = frame.size.height + 140
            
            /*if indexPath.section == commentList.count - 1 { // last cell
             if totalItems > commentList.count { //removing totalItems for always service call
             getComments()
             }
             }*/
        }
        return c
    }
    
    @objc func deleteAction(_ sender:UITapGestureRecognizer){
        commentId = (sender.view?.tag)!
        deleteComment()
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    
    @objc override func keyboardWasShown(notification: NSNotification){
        //Need to calculate keyboard exact size due to Apple suggestions
                
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            self.tableViewScrollToBottom(animated: true)

            let keyboardFrame: CGRect = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            stackBottom.constant = keyboardFrame.height
        }
    }
    
    @objc override func keyboardWillBeHidden(notification: NSNotification){
        //Once keyboard disappears, restore original positions
        self.view.endEditing(true)
        
        stackBottom.constant = 0.0
//        noCommentsBottom.constant = 15
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Upišite komentar"
            textView.textColor = UIColor.lightGray
        }
    }
    
    func insertComment(){
        
        let headers = [
            "Authorization": "ApiKey \(userList[0].apiKey)",
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        let comment = (commentView.text!.encodeEmoji)
        var error : String = ""
                
        let url : String = "http://cedisklik.me/ws/_addNewComment.php?commentText=\(comment.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)&photoID=\(photosList[photoSelection!].id)"
        
        Alamofire.request(url, method: .get, encoding: URLEncoding.httpBody, headers: headers)
            .validate()
            .responseJSON { (response) -> Void in
                
                if let requestData = response.result.value as! NSDictionary?{
                    
                    var code : String?
                    code = String(describing: requestData["error_code"]!)
                    
                    if code == "1" {
                        
                        error = String(describing: requestData["errors"]!)
                        
                        let alert1 = UIAlertController(title: "Greška", message: "Pokušajte ponovo!", preferredStyle: UIAlertControllerStyle.alert)
                        
                        alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  nil))
                        self.present(alert1, animated: true, completion: nil)
                        
                    }  else {
                        
                        self.commentList.removeAll()
                        self.getComments()
                        self.commentView.text = "Unesi komentar"
                        self.commentView.textColor = UIColor.lightGray
                        
                        //self.tableViewScrollToBottom(animated: true)
                    }
                } else {
                    let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                    alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { action in
                        //hud1.hide(animated: true, afterDelay: 0)
                    }))
                    self.present(alert9, animated: true, completion: nil)
                }
        }
    }
    
    /*func tableViewScrollToBottom(animated: Bool) {
     DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
     let numberOfSections = self.tableView.numberOfSections
     
     if numberOfSections > 0 {
     let numberOfRows = self.tableView.numberOfRows(inSection: numberOfSections-1)
     
     if numberOfRows > 0 {
     let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
     self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: animated)
     }
     }
     }
     }*/
    
    
    @objc func deleteComment(){
        
        
        if (CheckConnection.isInternetAvailable() == true) {
            
            
            let headers = [
                "Authorization": "ApiKey \(userList[0].apiKey)",
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            
            var error : String = ""
            let url : String = "http://cedisklik.me/ws/_deleteComment.php?commentID=\(commentId)"
            
            let alert = UIAlertController(title: "Brisanje komentara", message: "Da li ste sigurni da želite da izbrišete ovaj komentar?", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title: "Da", style: UIAlertActionStyle.default, handler: { action in
                
                
                Alamofire.request(url, encoding: URLEncoding.httpBody, headers: headers)
                    .validate()
                    .responseJSON { (response) -> Void in
                        
                        if let requestData = response.result.value as! NSDictionary?{
                            
                            var code : String?
                            code = String(describing: requestData["error_code"]!)
                            
                            if code == "1" {
                                error = String(describing: requestData["errors"]!)
                                
                                let alert1 = UIAlertController(title: "Greška", message: "Pokušajte ponovo!", preferredStyle: UIAlertControllerStyle.alert)
                                
                                alert1.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler:  nil))
                                self.present(alert1, animated: true, completion: nil)
                                
                            }
                            else {
                                self.commentList.removeAll()
                                self.getComments()
                            }
                            
                        }
                        else {
                            //hud.hide(animated: true, afterDelay: 0)
                            let alert9 = UIAlertController(title: "Obavještenje", message: "Greška u komunikaciji sa serverom!", preferredStyle: UIAlertControllerStyle.alert)
                            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { action in
                                //hud1.hide(animated: true, afterDelay: 0)
                            }))
                            self.present(alert9, animated: true, completion: nil)
                        }
                        
                }
                
                
            }))
            alert.addAction(UIAlertAction(title: "Otkaži", style: UIAlertActionStyle.cancel, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert9 = UIAlertController(title: "Obavještenje", message: "Provjerite internet konekciju!", preferredStyle: UIAlertControllerStyle.alert)
            alert9.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil ))
            self.present(alert9, animated: true, completion: nil)
        }
    }
    
    func tableViewScrollToBottom(animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            let numberOfSections = self.tableView.numberOfSections
            
            if numberOfSections > 0 {
                let numberOfRows = self.tableView.numberOfRows(inSection: numberOfSections-1)
                
                if numberOfRows > 0 {
                    let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                    self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                }
                
            }
        }
    }
}

extension String {
    var encodeEmoji: String{
        if let encodeStr = NSString(cString: self.cString(using: .nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue){
            return encodeStr as String
        }
        return self
    }
}

extension String {
    var decodeEmoji: String{
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        if let str = decodedStr{
            return str as String
        }
        return self
    }
}

